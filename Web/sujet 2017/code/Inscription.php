<?php
	namespace SocialBeerClub;
	
	class Inscription{
		private $correct = false;
		
		public function inscrire()
		{
			if (!isset($_POST['id']) || !isset($_POST['mdp']) || !isset($_POST['mdp-conf']) || !isset($_POST['pays']) || !isset($_POST['email']) || !isset($_POST['date']))
				return false;
			if ($_POST['mdp'] != $_POST['mdp-conf'])
				return false;
			
			$mdp = password_hash($_POST['mdp'], PASSWORD_DEFAULT);
			$personne = new Modele\Personne();
			$this->correct = $personne->ajouterPersonne($_POST['id'], $mdp, $_POST['email'], $_POST['date'], $_POST['pays']);
			return $this->correct;
		}
		
		public function render()
		{
			header("Location : /");
			return "";
		}
	}