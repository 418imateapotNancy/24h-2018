<?php
	namespace SocialBeerClub\Modele;
	
	class Personne extends Modele{
		
		public function __construct(){
			$this->table = "membres";
			$this->primaryKey = "idMembre";
			parent::__construct();
		}
		
		public function ajouterPersonne($ident, $mdp, $email, $dateNaiss, $pays){
			$sql = "INSERT INTO $this->table (identifiant, mdp, email, dateNaissance, pays) VALUES (:ident, :mdp, :email, :dateNaiss, :pays)";
			
			$req = Modele::$bdd->prepare($sql);
			$res = $req->execute(array(":ident" => $ident, ":mdp" => $mdp, ":email" => $email, ":dateNaiss" => $dateNaiss, ":pays" => $pays));
			
			if($res)
			{
				$id = Modele::$bdd->lastInsertId();
				$this->load($id);
			}
			
			return $res;
		}
		
		public function getBiereCree(){
			$tabB = $bieres->getAll();
			
			$var = $this;
			$tabBCree = array_filter($tabB, function($el) use ($var){
				return $el->createur == $var->idMembre;
			});
			
			return $tabBCree;
		}
		
		public function getBiereAimee(){
			$sql = "SELECT * FROM aimerBiere";
			$req = Modele::$bdd->prepare($sql);
			$res = $req->execute();
			
			if(res){
				$tabB = $req->fetchAll();
				
				$var = $this;
				$tabBCree = array_filter($tabB, function($el) use ($var){
					return $el->idMembre == $var->idMembre;
				});
			
				return $tabBCree;
			} else{
				return array();
			}
		}
		
		public function getAmis(){
			$sql = "SELECT * FROM amitie";
			$req = Modele::$bdd->prepare($sql);
			$res = $req->execute();
			
			if($res){
				$tabA = $req->fetchAll();
				
				$var = $this;
				$tabACree = array_filter($tabA, function($el) use ($var){
					return $el['idAmi'] == $var->idMembre;
				});
			
				return $tabACree;
			} else{
				return array();
			}
		}
		
		public function getAvis(){
			$req = Modele::$bdd->prepare("SELECT * FROM avis WHERE idMembre = :id");
			$res = $req->execute(array(":id" => $this->idMembre));
			return $res ? $req->fetchAll() : array();
		}
	}
?>
