<?php 
	namespace SocialBeerClub\Modele;

	class Modele{
		private static $tables = ['biere', 'membres', 'avis'];
		protected static $bdd = null;
		protected $table;
		protected $primaryKey;
		protected $properties;
		
		public function __construct(){
			if ($this->table == null || $this->primaryKey == null || !in_array($this->table, Modele::$tables))
				throw new \Exception("Modele incorrect");
			
			if ($this->bdd == null)
				$this->loadBdd();
		}
		
		private static function loadBdd()
		{
			Modele::$bdd = New \PDO('mysql:host=localhost;dbname=beersocialclub;charset=utf8','root','sggZgV7xdYCeg3BF');
			
			Modele::$bdd->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_WARNING);
			Modele::$bdd->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
			Modele::$bdd->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
		}
		
		public function __get($name)
		{
			if (isset($this->properties[$name]))
				return $this->properties[$name];
			return null;
		}
		
		public function getAll()
		{
			$req = Modele::$bdd->prepare("SELECT * FROM {$this->table}");
			$res = $req->execute();
			return $res ? $req->fetchAll() : array();
		}
		
		public function rechercher($cols, $terms)
		{
			$sql = "SELECT * FROM {$this->table} WHERE ";
			$data = [];
			foreach ($cols as $c)
			{
				foreach ($terms as $t)
				{
					$sql .= " $c LIKE ? OR";
					array_push($data, "%$t%");
				}
			}
			$sql = substr($sql, 0, -2);
			$req = Modele::$bdd->prepare($sql);
			$res = $req->execute($data);
			return $res ? $req->fetchAll() : array();
		}
		
		public function load($id)
		{
			$req = Modele::$bdd->prepare("SELECT * FROM {$this->table} WHERE {$this->primaryKey} = :id");
			$res = $req->execute(array(":id" => $id));
			if (!$res)
				throw new \Exception("Erreur de requete");
			$this->properties = $req->fetch();
		}
		
		public function update($props)
		{
			$query="UPDATE {$this->table} SET ";
			
			$prop_names = array_keys($props);
			$size = count($prop_names);
			
			
			foreach($prop_names as $key => $prop)
			{
				//Check if prop_names is a legal column
				$query .= $prop."=:".$prop.($key!=$size-1?", ":" "); //"prop=:prop, " ou "prop=:prop " si c'est le dernier element
			}
			
			$query .= "WHERE {$this->primaryKey}=:id";
			
			$req = Modele::$bdd->prepare($query);
			$params = array_merge(array(":id"=>$this->properties[$primaryKey]), $props);
			return $req->execute($params);
		}
		
		public function delete()
		{
			$req = Modele::$bdd->prepare("DELETE FROM {$this->table} WHERE {$this->primaryKey} = :id");
			return $req->execute(array(":id"=>$this->properties[$primaryKey]));
		}
	}
