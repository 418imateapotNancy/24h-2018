<?php
	namespace SocialBeerClub\Modele;
	
	class Biere	extends Modele{
		
		public function __construct(){
			$this->table = "biere";
			$this->primaryKey = "idBiere";
			parent::__construct();
		}
		
		public function ajouterBiere($nom, $type, $degres, $brasseur, $createur, $image){
			$sql = "INSERT INTO $this->table (nom, type, degres, brasseur, createur, image) VALUES (:idBiere, :nom, :type, :degres, :brasseur, :createur, :image)";
			
			$req = Modele::$bdd->prepare($sql);
			$res = $req->execute(array(":nom" => $nom, ":type" => $type, ":degres" => $degres, ":brasseur" => $brasseur, ":createur" => $createur, ":image" => $image));
			
			if($res)
				$id = $this->table->lastInsertId();
			
			return $res;
		}
		
		public function getAvis(){
			$req = Modele::$bdd->prepare("SELECT * FROM avis WHERE idBiere = :id");
			$res = $req->execute(array(":id" => $this->idBiere));
			return $res ? $req->fetchAll() : array();
		}
	}
?>