<?php
	namespace SocialBeerClub\Modele;

	class Avis extends Modele{

		public function __construct()
		{
			$this->primaryKey = "idAvis";
			$this->table = "avis";
			parent::__construct();
		}

		public function ajouterAvis($idMembre, $idBiere, $commentaire, $note)
		{
			$req = Modele::$bdd->prepare("INSERT INTO {$this->table} (idMembre, idBiere, commentaire, note) VALUES (:idM, :idB, :co, :note)");
			$res = $req->execute(array(":idM" => $idMembre, ":idB" => $idBiere, ":co" => $commentaire, ":note" => $note));
			if (!$res)
				return $res;
			
			$this->load(Modele::$bdd->lastInsertId());
			return true;
		}
	}
