<?php
	namespace SocialBeerClub\Modele;
	
	class Utilisateur extends Personne{
		
		public $log, $bieres;
		
		public function __construct(){
			$bieres = new Biere();
			parent::__construct();
		}
		
		public function ajouterAmis($amis){
			$sql = "INSERT INTO $this->amitie (idMembre, idAmi) VALUES (:idMembre, :idAmi)";
			
			$req = Modele::$bdd->prepare($sql);
			
			foreach($amis as $ami){
				$res = $req->execute(array(":idMembre" => $this->idMembre, ":idAmi" => $ami));
				
				if(! $res){
					return false;
				}
			}
				
			foreach($amis as $ami){
				$res = $req->execute(array(":idMembre" => $ami, ":idAmi" => $this->idMembre));
				
				if(! $res){
					return false;
				}
			}
			
			return $res;
		}
		
		public function login($id, $mdp){
			session_start();
			
			if(! $this->log){
				
				$sql = "SELECT mdp, idMembre FROM membres WHERE identifiant = :id";
				$req = Modele::$bdd->prepare($sql);
				$res = $req->execute(array(":id" => $id));
				
				if($res){
					$data = $req->fetch();
					$res = password_verify($mdp, $data["mdp"]);
					if (($this->log = $res))
					{
						$this->load($data["idMembre"]);
						$_SESSION['utilisateur'] = $this;
					}			
					return $res;
				} else{
					return false;
				}
			} else{
				return false;
			}
		}
		
		public function logout(){
			session_start();
			
			if($this->log){
				$this->log = false;
				unset($_SESSION['utilisateur']);
				
				return true;
			} else{
				return false;
			}
		}
		
		public function ajouterBiere($nom, $type, $degres, $brasseur, $image){
			if($nom != null){
				$res = $bieres->ajouterBiere($nom, $type, $degres, $brasseur, $this->idMembre, $image);
			
				return $res;
			} else{
				return false;
			}
		}
		
		public function ajouterAvis($idBiere, $commentaire, $note)
		{
			return (new Avis())->ajouterAvis($this->idMembre, $idBiere, $commentaire, $note);
			
		}
		
		public function modifierBiere($id, $props){
			try{
				$bieres->load($id);
			} catch(Exception $e){
				return false;
			}
			
			$props = array_merge(array("idBiere" => $id), $props);
			
			if(($props["nom"] != $bieres->nom) and ($bieres->createur != $this->idMembre)){
				$props["nom"] = $bieres->nom;
			}
			if((($props["type"] != $bieres->type) and ($bieres->createur != $this->idMembre)) or ($bieres->type != null)){
				$props["type"] = $bieres->type;
			}
			if((($props["degres"] != $bieres->degres) and ($bieres->createur != $this->idMembre)) or ($bieres->degres != null)){
				$props["degres"] = $bieres->degres;
			}
			if((($props["brasseur"] != $bieres->brasseur) and ($bieres->createur != $this->idMembre)) or ($bieres->brasseur != null)){
				$props["brasseur"] = $bieres->brasseur;
			}
			if((($props["image"] != $bieres->image) and ($bieres->createur != $this->idMembre)) or ($bieres->image != null)){
				$props["image"] = $bieres->image;
			}
			
			$bieres = new Biere(); 			// ?
			$res = $bieres->update($props);
			return res;
		}
		
		public function aimerBiere($idBieres){
			$sql = "INSERT INTO aimerBiere (idMembre, idBiere) VALUES (:idM, :idB)";
			$req = Modele::$bdd->prepare($sql);
			$res = $req->execute(array(":idM" => $this->idMembre, ":idB" => $bieres->idBieres));
			
			return $res;
		}
		
		
	}
?>
