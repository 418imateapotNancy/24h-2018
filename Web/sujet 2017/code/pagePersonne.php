<?php
	namespace SocialBeerClub;

	class PagePersonne{

    private $personne=null;
    private $avirecent;
    /*private $id="inconnu";
    private $pays ="inconnu";
    private $date = "inconnu";
    private $biere = "";
    private $avis = "";*/
		public function render($identifiant)
		{
      /*$id=$identifiant;
      $req = Modele::$bdd->prepare("SELECT * FROM {membres} WHERE {indentifiant} = :ident");
			$res = $req->execute(array(":ident" => $identifiant));*/
      $p=new Modele\Personne();
      $personnes= $p->getAll();
      $this->avisrecent = null;

	  $idMembre = -1;
      foreach ($personnes as $pers) {
        if($pers['identifiant'] == $identifiant)
          $idMembre = $pers['idMembre'];
      }
	  $this->personne = new Modele\Personne();
	  $this->personne->load($idMembre);
      foreach ($this->personne->getAvis() as $avi) {
        if($avi['dateAjout'] > $this->avisrecent['dateAjout'] || $this->avisrecent == null)
          $this->avirecent=$avi;
      }
      /*
      if($res){
        $res->$pays;
      }*/
			$template = file_get_contents($_SERVER['DOCUMENT_ROOT']."/vues/PagePersonne.html");

			$html = file_get_contents($_SERVER['DOCUMENT_ROOT']."/vues/header.html");
			$html .= preg_replace_callback("/%([A-Z]+)/", array($this, "callback"),$template);
			$html .= file_get_contents($_SERVER['DOCUMENT_ROOT']."/vues/footer.html");

			return $html;
		}

		private function callback($m)
		{
				switch($m[1])
				{
					case 'IDENTIFIANT':
						return $this->personne->identifiant;
					case 'PAYS':
							return $this->personne->pays;
						case 'DATENAISSANCE':
						 	return $this->personne->dateNaissance;
						case 'NOMBIERE':
							$biere = new Modele\Biere();
							$biere->load($this->avirecent['idBiere']);
							return $biere->nom;
						case 'AVIS':
							return $this->avirecent['commentaire'];
				}
		}
	}
