<?php
	namespace SocialBeerClub;

	class VueBiere{
		private $biere;
		private $nomBiere;
		private $nomCommentaire;
		private $avis;
		
		public function __construct(string $nom)
		{
			$this->nomBiere = $nom;
			$b = new Modele\Biere();
			$bieres = $b->getAll();
			$this->biere = null;
			foreach($bieres as $b)
			{
				if ($b['nom'] == $nom)
				{
					$this->biere = $b;
					break;
				}
			}
		}
		
		public function render()
		{
			if ($this->biere == null)
				$template = "<div class='alert alert-danger'>La biere $this->nomBiere n'existe pas !</div>";
			else
				$template = file_get_contents($_SERVER['DOCUMENT_ROOT']."/vues/pageBiere.html");
			
			$html = file_get_contents($_SERVER['DOCUMENT_ROOT']."/vues/header.html");
			$html .= preg_replace_callback("/%([A-Z]+)/", array($this, "callback"), $template);
			$html .= file_get_contents($_SERVER['DOCUMENT_ROOT']."/vues/footer.html");

			return $html;
		}

		private function callback($m)
		{
				switch($m[1])
				{
						case 'IDBIERE':
							return $this->biere['idBiere'];
						case 'IMAGEBIERE':
							return $this->biere['image'];
						case 'NOMBIERE':
						 	return $this->biere['nom'];
						case 'DEGREBIERE':
						 	return $this->biere['degre'];
						case 'BRASSEUR':
						 	return $this->biere['brasseur'];
						case 'NOTEMOYENNE':
						 	return $this->biere['noteMoyenne'];
						case 'TYPE':
						 	return $this->biere['type'];
						case 'DATEAJOUT':
						 	return $this->biere['dateAjout'];
						case 'COMMENTAIRES':
							return $this->renderCommentaires();
						case 'NOM':
							return $this->nomCommentaire;
						case 'NOTE':
							return $this->avis['note'];
						case 'COMMENTAIRE':
							return $this->avis['commentaire'];
				}
		}
		
		public function renderCommentaires()
		{
			$commentaire = file_get_contents($_SERVER['DOCUMENT_ROOT']."/vues/commentaire.html");
							
			$biereClass = new Modele\Biere();
			$biereClass->load($this->biere['idBiere']);
			$html = "";
			foreach ($biereClass->getAvis() as $avis) 
			{
				$this->avis = $avis;
				$p = new Modele\Personne();
				$p->load($avis['idMembre']);
				$this->nomCommentaire = $p->identifiant;
				$html .= preg_replace_callback("/%([A-Z]+)/", array($this, "callback"), $commentaire);
			}
			return $html;
		}
	}
