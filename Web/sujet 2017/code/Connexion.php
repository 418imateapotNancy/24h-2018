<?php
	namespace SocialBeerClub;
	
	class Connexion{
		private $correct = false;
		
		public function connecter()
		{
			if (!isset($_POST['identifiant']) || !isset($_POST['mdp']))
				return false;
			
			$u = new Modele\Utilisateur();
			$this->correct = $u->login($_POST['identifiant'], $_POST['mdp']);
			return $this->correct;
		}
		
		public function render()
		{
			header("Location : /");
			return "";
		}
	}