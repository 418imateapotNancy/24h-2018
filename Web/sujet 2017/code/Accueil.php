<?php
	namespace SocialBeerClub;
	class Accueil{
		
		public function render()
		{
			$html = file_get_contents($_SERVER['DOCUMENT_ROOT']."/vues/header.html");
			$html .= file_get_contents($_SERVER['DOCUMENT_ROOT']."/vues/accueil.html");
			$html .= file_get_contents($_SERVER['DOCUMENT_ROOT']."/vues/footer.html");
			return $html;
		}
	}