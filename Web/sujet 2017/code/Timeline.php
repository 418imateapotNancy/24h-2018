<?php
	namespace SocialBeerClub;

	class Timeline{
		private $util;
		private $avisrecent=null;
		private $classement=[];
		private $biere = null;
		
		public function render(Modele\Utilisateur $u)
		{
			$this->biere = new Modele\Biere();
			$this->avisrecent = new Modele\Avis();
			
			$this->util=$u;
			$template = file_get_contents($_SERVER['DOCUMENT_ROOT']."/vues/FileActu.html");
			
			$html = file_get_contents($_SERVER['DOCUMENT_ROOT']."/vues/header.html");
			$html .= preg_replace_callback("/%([A-Z]+)/", array($this, "callback"),$template);
			$html .= file_get_contents($_SERVER['DOCUMENT_ROOT']."/vues/footer.html");

			return $html;
		}

		private function callback($m)
		{
				switch($m[1])
				{
					case 'FILACTU':
						$biere = file_get_contents($_SERVER['DOCUMENT_ROOT']."/vues/avis.html");
						$filActu = "";
						foreach ($this->util->getAmis() as $ami) {
							$amiClass = new Modele\Personne();
							$amiClass->load($ami['idMembre']);
							foreach ($amiClass->getAvis() as $avi) {
								array_push($this->classement, $avi);
							}
						}
						$index=0;
						$size = min(count($this->classement), 20);
						for($i=0; $i<$size; $i++){
							$this->avisrecent=$this->classement[0];
							foreach ($this->classement as $key => $res) {
								if($res['dateAjout'] >= $this->avisrecent['dateAjout']){
									$this->avisrecent=$res;
									$index=$key;
								}
							}
							$this->biere->load($this->avisrecent['idBiere']);
							$filActu .= preg_replace_callback("/%([A-Z]+)/", array($this, "callback"), $biere);
							array_splice($this->classement, $index);
							$index=0;
							$avisrecent=new Modele\Avis();
						}
						$this->classement = [];
						return $filActu;
						case 'IMAGEBIERE':
							return $this->biere->image;
						case 'NOMBIERE':
						 	return $this->biere->nom;
						case 'NOMAMI':
							$ami = new Modele\Personne();
							$ami->load($this->avisrecent['idMembre']);
							return $ami->identifiant;
						case 'DESCBIERE':
							return $this->avisrecent['commentaire'];
				}
		}
	}
