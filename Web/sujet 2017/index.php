<?php
	require_once("vendor/autoload.php");
	
	$app = new \Slim\Slim();
	
	//Views
	$app->get("/", function(){
		session_start();
		if (isset($_SESSION['utilisateur']))
		{
			$u = $_SESSION['utilisateur'];
			$timeline = new SocialBeerClub\Timeline();
			$html = $timeline->render($u);
		}
		else
		{
			$accueil = new SocialBeerClub\Accueil();
			$html = $accueil->render();
		}
		echo $html;
	});
	
	$app->get("/biere/:nom", function($nom){
		session_start();
		if (isset($_SESSION['utilisateur']))
		{
			$b = new SocialBeerClub\VueBiere($nom);
			$html = $b->render();
		}
		else
			$app->redirect("/");
		echo $html;
	})->name('biere');
	
	$app->get("/personne/:identifiant", function($identifiant){
		session_start();
		if (isset($_SESSION['utilisateur']))
		{
			$b = new SocialBeerClub\PagePersonne();
			$html = $b->render($identifiant);
		}
		else
			$app->redirect("/");
		echo $html;
	})->name("personne");
	
	
	//Forms
	$app->post("/inscription", function() use ($app){
		$ins = new SocialBeerClub\Inscription();
		$ins->inscrire();
		$html = $ins->render();
		if (!$html)
			$app->redirect("/");
		else
			echo $html;
	});
	
	$app->post("/connexion", function() use ($app){
		$co = new SocialBeerClub\Connexion();
		$co->connecter();
		$html = $co->render();
		if (!$html)
			$app->redirect("/");
		else
			echo $html;
	});
	
	//Ajax
	$app->post("/ajouter/:type", function($type) use ($app){
		session_start();
		$err = "no";
		if (!isset($_SESSION['utilisateur']))
		{
			$err = "deconnecte";
		}
		else{
			$u = $_SESSION['utilisateur'];
			switch($type)
			{
				case "biere":
					if (!isset($_POST['nom']) || !isset($_POST['type']) || !isset($_POST['degres']) || !isset($_POST['brasseur']) || !isset($_FILES['img']) || count($_FILES['img']['error']) != 1)
						$err = "manque";
					else
					{
						if ($_FILES['img']['error'][0] != UPLOAD_ERR_OK)
							$err = "transfert";
						else
						{
							$name = $_FILES['img']['name'][0];
							$ext = substr($name, strrpos(".", $name) + 1);
							$path = "/images/$nom.$ext";
							$absPath = "$_SERVER[DOCUMENT_ROOT]$path";
							if (!move_uploaded_file($_FILES['img']['tmp_name'][0],  $absPath))
								$err = "fichier";
							else
								$err = $u->ajouterBiere($_POST['nom'], $_POST['type'], $_POST['degres'], $_POST['brasseur'], $path) ? "no" : "ajouter";
						}
					}
					break;	
				case "avis":
					if (!isset($_POST['idBiere']) || !isset($_POST['commentaire']) || !isset($_POST['note']) || !is_numeric($_POST['idBiere']) || !is_numeric($_POST['note']))
						$err = "manque";
					else
						$err = $u->ajouterAvis(intval($_POST['idBiere']), $_POST['commentaire'], $_POST['note'])  ? "no" : "ajouter";
					break;	
				default:
					$err = "incorrect";
			}
		}
		echo json_encode(array("err"=> $err));
	});
	
	$app->post("/aimer/:type/:id", function($type, $id) use ($app){
		$err = "no";
		if (isset($_SESSION['utilisateur']))
		{
			$err = "deconnecte";
		}
		else{
			$u = $_SESSION['utilisateur'];
			switch($type)
			{
				case "biere":
					$err = $u->aimerBiere($id) ? "no" : "aimer";
					break;	
				case "membre":
					$err = $u->ajouterAmis(array($id)) ? "no" : "aimer";
					break;	
				default:
					$err = "incorrect";
			}
		}
		echo json_encode(array("err"=> $err));
	});
	
	$app->post("/modifier/:type/:id", function($type, $id) use ($app){
		$err = "no";
		if (isset($_SESSION['utilisateur']))
		{
			$err = "deconnecte";
		}
		else{
			$u = $_SESSION['utilisateur'];
			switch($type)
			{
				case "biere":
					if (array_intersect($_POST, array("nom", "type", "brasseur", "dateAjout", "degre")) != $_POST)
						return json_encode(array("err" => "incorrect"));
					$err = $u->modifierBiere($id, $_POST) ? "no" : "aimer";
					break;	
				default:
					$err = "incorrect";
			}
		}
		echo json_encode(array("err"=> $err));
	});
	
	$app->get("/recherche/:type/:terme", function($type, $terme) use ($app) {
		$err = "no";
		$array = [];
		if (isset($_SESSION['utilisateur']))
		{
			$err = "deconnecte";
		}
		else{
			switch($type)
			{
				case "biere":
					$array = (new SocialBeerClub\Modele\Biere())->rechercher(array("nom", "brasseur"), array($terme));
					break;
				case "personne":
					$array = (new SocialBeerClub\Modele\Personne())->rechercher(array("identifiant"), array($terme));
					break;
				case "avis":
					$array = (new SocialBeerClub\Modele\Avis())->rechercher(array("commentaire"), array($terme));
					break;
				case "tous":
					$array = array( "bieres" => (new SocialBeerClub\Modele\Biere())->rechercher(array("nom", "brasseur"), array($terme)),
									"utilisateurs" => (new SocialBeerClub\Modele\Personne())->rechercher(array("identifiant"), array($terme)),
									"avis" => (new SocialBeerClub\Modele\Avis())->rechercher(array("commentaire"), array($terme))
								);
					foreach($array["avis"] as &$b)
					{
						$biere = new SocialBeerClub\Modele\Biere();
						$biere->load($b['idBiere']);
						$b['nombiere'] = $biere->nom;
					}
					unset($b);
					foreach($array["bieres"] as &$b)
					{
						$b['href'] = $app->urlFor("biere", array('nom' => $b['nom']));
					}
					unset($b);
					foreach($array["utilisateurs"] as &$b)
					{
						$b['href'] = $app->urlFor("personne", array("identifiant" => $b['identifiant']));
					}
					$array['count'] = count($array['bieres']) + count($array['utilisateurs']) + count($array['avis']);
					$err = count($array['bieres']) + count($array['utilisateurs']) + count($array['avis']) > 0 ? "no" : "no data";
					break;
				default:
					$err = "incorrect";
			}
		}
		if (count($array) == 0)
			$err = "no data";
		echo json_encode(array("err"=> $err, "data" => $array));
	});
	
	/*$app->post("/supprimer/:type/:id", function($type, $id) use ($app){
		$err = "no";
		if (isset($_SESSION['utilisateur']))
		{
			$err = "deconnecte";
		}
		else{
			$u = $_SESSION['utilisateur'];
			switch($type)
			{
				case "biere":
					$err = $u->aimerBiere($id) ? "no" : "aimer";
					break;	
				case "ami":
					$err = $u->ajouterAmis(array($id)) ? "no" : "aimer";
					break;	
				default:
					$err = "incorrect";
			}
		}
		echo json_encode(array("err"=> $err));
	});*/
	
	$app->run();
	