$("form.ajouterAvis").submit(function(e){
		e.preventDefault();
		let form = $(e.target);
		let com = form.find("[name=commentaire]").val();
		let note = form.find("[name=note]").val();
		let idBiere = form.find("[name=idBiere]").val();
		
		$.ajax("/ajouter/avis", {
				method : "POST",
				data : {
					commentaire: com,
					note : note,
					idBiere: idBiere
				},
				complete: function(data, status)
				{
					if (status == 'success')
					{
						let o = JSON.parse(data.responseText);
						console.log(o.err);
					}
					else
						console.log("Ajax error : "+status);
				}
			});
	});

$("#recherche").on("keyup", function(e) {
	let finished = false;
	let val = $(e.target).val();
	if (!val.length)
	{
		finished = true;
		$("#resultatRecherche").stop(true, true).slideUp();
		return;
	}
	finished = false;
	$.ajax("/recherche/tous/" + val, {
		method : "GET",
		complete : function(data, status){
			if (finished)
				return;
			if (status == 'success')
			{
				let o = JSON.parse(data.responseText);
				if (o.err != "no" && o.err != "no data")
					console.log(o.err);
				else
				{
					let html = "";
					console.log(o);
					html += "<div class = 'row'>";
						html += "<div class='col d-flex justify-content-center align-items-center'><h3>Bières</h3></div>";
					html += "</div>";
					if (o.data.bieres.length == 0)
					{
						html += "<div class = 'row'>";
							html += "<div class='col d-flex justify-content-center align-items-center'>Oh zut ! Pas de bière ici</div>";
						html += "</div>";
					}
					for(let row of o.data.bieres)
					{
						html += "<a href="+row.href+"><div class='row text-center py-2'>"
							html += "<div class='col d-flex justify-content-center align-items-center'><img src='"+row.image+"' height='50'></div>";
							html += "<div class='col d-flex justify-content-center align-items-center'>"+row.nom+"</div>";
							html += "<div class='col d-flex justify-content-center align-items-center'>"+row.degre+"°</div>";
						html += "</div></a>";
					}
					
					html += "<div class = 'row mt-2'>";
						html += "<div class='col d-flex justify-content-center align-items-center'><h3>Utilisateurs</h3></div>";
					html += "</div>";
					if (o.data.utilisateurs.length == 0)
					{
						html += "<div class = 'row mt-2'>";
							html += "<div class='col d-flex justify-content-center align-items-center'>Pas besoins d'amis quand on a une bonne bière</div>";
						html += "</div>";
					}
					for(let row of o.data.utilisateurs)
					{
						html += "<a href="+row.href+"><div class='row text-center py-2'>"
							html += "<div class='col d-flex justify-content-center align-items-center'>"+row.identifiant+"</div>";
						html += "</div></a>";
					}
					
					html += "<div class = 'row mt-2'>";
						html += "<div class='col d-flex justify-content-center align-items-center'><h3>Avis</h3></div>";
					html += "</div>";
					if (o.data.avis.length == 0)
					{
						html += "<div class = 'row'>";
							html += "<div class='col d-flex justify-content-center align-items-center'>De toute façon les autres n'ont aucun goût</div>";
						html += "</div>";
					}
					for(let row of o.data.avis)
					{
						html += "<div class='row text-center py-2'>"
							html += "<div class='col d-flex justify-content-center align-items-center'>"+row.nombiere+"</div>";
							html += "<div class='col d-flex justify-content-center align-items-center'>"+row.note+"/5</div>";
							html += "<div class='col d-flex justify-content-center align-items-center'>"+row.commentaire+"</div>";
						html += "</div>";
					}
					$("#resultatRecherche").html(html).stop(true, true).slideDown();
				}
			}
			else
				console.log("Ajax error : "+status);
		}
	});
});