package competiteur;

import a.j;
import a.m;
import a.o;
import d.b.a;
import java.io.PrintStream;

public class GrandOrdoCompetiteur
{
  public GrandOrdoCompetiteur() {}
  
  private static String a(String[] paramArrayOfString)
  {
    StringBuffer localStringBuffer;
    (localStringBuffer = new StringBuffer()).append("arguments : ");
    paramArrayOfString = paramArrayOfString;
    for (int i = 0; i < 2; i++)
    {
      String str = paramArrayOfString[i];
      localStringBuffer.append(str).append(" ");
    }
    return localStringBuffer.toString();
  }
  
  public static void main(String[] paramArrayOfString)
  {
    String[] arrayOfString = { "portGrandOrdo", "repJournaux" };
    if (paramArrayOfString.length < 2)
    {
      System.out.println(a(arrayOfString) + "[options de la phase]");
      System.exit(1);
    }
    m localM = m.c;
    String str;
    j.a(str = o.a("repJournaux", arrayOfString, paramArrayOfString));
    int i = o.b("portGrandOrdo", arrayOfString, paramArrayOfString);
    a localA;
    (localA = new a("localhost", i, null, -1)).a(paramArrayOfString, arrayOfString, localM);
    localA.a(paramArrayOfString, arrayOfString, true, false, false);
  }
}
