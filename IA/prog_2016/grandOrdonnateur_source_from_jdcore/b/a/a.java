package b.a;

import a.c;
import a.f;
import a.k;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class a
  extends f
{
  public a() {}
  
  public a(k paramK, a.b paramB, c paramC, Point paramPoint)
  {
    super(paramK, paramB, paramC, paramPoint);
  }
  
  public final String e()
  {
    return "« intelligence artificielle » " + b.a.a();
  }
  
  public final String f()
  {
    return "« intelligence artificielle » " + b.a.b();
  }
  
  public final a.a o()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = a.a.e().iterator();
    while (localIterator.hasNext())
    {
      a.a localA = (a.a)localIterator.next();
      if (a.a(b, localA.d())) {
        localArrayList.add(localA);
      }
    }
    return a(localArrayList);
  }
}
