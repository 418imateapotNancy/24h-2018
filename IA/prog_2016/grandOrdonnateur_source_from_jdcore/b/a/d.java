package b.a;

import a.j;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

public class d
{
  private static final Logger a = Logger.getLogger(d.class.getName());
  private Date b = new Date();
  private String c = null;
  private a.d d;
  private Integer e = null;
  private Integer f = null;
  private Integer g = null;
  private String h = null;
  private List i = new ArrayList();
  private Integer j = null;
  private List k = new ArrayList();
  
  public d(String paramString)
  {
    c = paramString;
    j.a(a);
    j();
  }
  
  private void j()
  {
    String str1 = null;
    String str2 = null;
    d.c.d localD = null;
    try
    {
      Object localObject1;
      if (((localObject1 = a(c)) != null) && ((localObject1 = a((File)localObject1)) != null) && ((localObject1 = a((Document)localObject1)) != null))
      {
        Object localObject2;
        Object localObject3;
        if ((localObject2 = a((Element)localObject1, "equipe")) == null)
        {
          k.add(new e("l'équipe n'est pas spécifiée"));
        }
        else
        {
          str1 = a((Element)localObject2, "courriel", "le courriel de l'équipe n'est pas spécifié");
          localObject3 = "a-zàâäçéèêëîïôöùûüÿæñA-ZÀÂÄÇÉÈÊËÎÏÔÖÙÛÜÆÑ";
          if ((str1 != null) && (!Pattern.matches("^[" + (String)localObject3 + "0-9._%+-]+@[" + (String)localObject3 + "0-9.-]+\\.[" + (String)localObject3 + "]{2,4}$", str1)))
          {
            k.add(new e("le courriel de l'équipe (" + ((Element)localObject2).getAttributeValue("courriel") + ") n'est pas au format d'une adresse électronique"));
            str1 = null;
          }
          if ((str1 != null) && (str1.length() > 1000))
          {
            k.add(new e("le courriel de l'équipe (" + str1 + ") doit être d'au plus 1000" + " caractères (et non de " + str1.length() + ")"));
            str1 = null;
          }
          if (((str2 = a((Element)localObject2, "commande", "la commande de l'équipe n'est pas spécifiée")) != null) && (str2.length() > 1000))
          {
            k.add(new e("la commande de l'équipe (" + null + ") doit être d'au plus 1000" + " caractères (et non de " + str2.length() + ")"));
            str2 = null;
          }
          if (((localObject2 = a((Element)localObject2, "systeme", "le système d'exploitation de l'équipe n'est pas spécifié")) != null) && (((String)localObject2).length() > 1000))
          {
            k.add(new e("le système d'exploitation de l'équipe (" + null + ") doit être d'au plus 1000" + " caractères (et non de " + ((String)localObject2).length() + ")"));
            localObject2 = null;
          }
          localD = d.c.d.a((String)localObject2);
          if ((localObject2 != null) && (localD == null)) {
            k.add(new e("le système d'exploitation doit être " + d.c.d.a + " ou " + d.c.d.b + " et non pas " + localD));
          }
        }
        int i2;
        if ((localObject3 = a((Element)localObject1, "cave")) == null)
        {
          k.add(new e("la cave n'est pas spécifiée"));
        }
        else
        {
          e = a(((Element)localObject3).getAttributeValue("nombre_lignes"), "le nombre de lignes", 1, 32);
          f = a(((Element)localObject3).getAttributeValue("nombre_colonnes"), "le nombre de colonnes", 1, 32);
          if ((e != null) && (f != null))
          {
            g = new Integer(e.intValue() * f.intValue());
            if ((g.intValue() < 2) || (g.intValue() > 400)) {
              k.add(new e("le nombre de salles de la cave (" + g.intValue() + ") doit être compris entre 2" + " et 400"));
            }
          }
          h = a((Element)localObject3, "casiers", "les casiers (nombre d'emplacements et position de l'escalier) de la cave ne sont pas spécifiés");
          if (h != null) {
            if (!h.matches("[0-9]*@[0-9]*"))
            {
              k.add(new e("les casiers (nombre d'emplacements et position de l'escalier) de la cave (" + h + ") ne respectent pas le format \"chiffres@" + "chiffres\""));
              h = null;
            }
            else if (g != null)
            {
              int m = 0;
              int n = 0;
              int i1 = 0;
              for (i2 = 0; i2 < h.length(); i2++)
              {
                char c1;
                if (Character.isDigit(c1 = h.charAt(i2)))
                {
                  m++;
                  int i3 = Character.getNumericValue(c1);
                  i1 += i3;
                  if ((i3 >= 0) && (i3 <= 9)) {
                    n++;
                  }
                }
              }
              if (m + 1 != g.intValue())
              {
                k.add(new e("les casiers (nombre d'emplacements et position de l'escalier) de la cave (" + m + ") ne correspondent pas au nombre de salles (" + g.intValue() + ")"));
                h = null;
              }
              else if (n != m)
              {
                k.add(new e("les casiers (nombre d'emplacements et position de l'escalier) de la cave contiennent " + (m - n) + " chiffre(s) qui doiv(en)t être compris entre 0" + " et 9"));
                h = null;
              }
              if ((i1 <= 0) || (i1 > 1600))
              {
                k.add(new e("le nombre total d'emplacements (" + i1 + ") doit être compris entre 1" + " et 1600"));
                h = null;
              }
            }
          }
        }
        Element localElement1;
        Object localObject4;
        if ((localElement1 = a((Element)localObject1, "manutentionnaires")) == null)
        {
          k.add(new e("les manutentionnaires ne sont pas spécifiés"));
        }
        else
        {
          if (((localObject4 = localElement1.getChildren("manutentionnaire")).size() < 2) || (((List)localObject4).size() > 3)) {
            k.add(new e("le nombre de manutentionnaires (" + ((List)localObject4).size() + ") doit être compris entre 2" + " et 3"));
          }
          Iterator localIterator = ((List)localObject4).iterator();
          i2 = 0;
          while (localIterator.hasNext())
          {
            Element localElement2;
            if (((localElement2 = (Element)localIterator.next()).getAttributeValue("nom") == null) || (localElement2.getAttributeValue("nom").isEmpty()))
            {
              i2++;
              i.add(null);
            }
            else if (!b.c().contains(localElement2.getAttributeValue("nom")))
            {
              k.add(new e("le nom de l'« intelligence artificielle » (" + localElement2.getAttributeValue("nom") + ") n'est pas correct et devrait être parmi " + b.c()));
            }
            else
            {
              i.add(localElement2.getAttributeValue("nom"));
            }
          }
          if (i2 != 1) {
            k.add(new e("le nombre de manutentionnaires correspondant à votre application (" + i2 + ") doit être de 1"));
          }
        }
        if ((localObject4 = a((Element)localObject1, "tours")) == null) {
          k.add(new e("le nombre de tours désiré n'est pas spécifié"));
        } else {
          j = a(((Element)localObject4).getAttributeValue("nombre_tours_desire"), "le nombre de tours désiré", 1, 20202020);
        }
      }
    }
    catch (Exception localException)
    {
      k.add(new e("problème survenu durant la lecture d'une partie décrite dans un fichier XML"));
    }
    if (!k.isEmpty()) {
      throw new f(k);
    }
    d = new a.d(null, -1, null, str1, str2, localD);
  }
  
  private File a(String paramString)
  {
    File localFile = null;
    try
    {
      localFile = new File(paramString);
    }
    catch (Exception localException)
    {
      k.add(new e("\"" + paramString + "\" n'est pas un nom de fichier correct"));
    }
    return localFile;
  }
  
  private Document a(File paramFile)
  {
    Document localDocument = null;
    try
    {
      localDocument = new SAXBuilder().build(paramFile);
    }
    catch (JDOMException|IOException localJDOMException)
    {
      k.add(new e("impossible d'accéder au fichier \"" + c + "\" ou de le considérer comme du XML"));
    }
    return localDocument;
  }
  
  private Element a(Document paramDocument)
  {
    Element localElement = null;
    try
    {
      localElement = paramDocument.getRootElement();
    }
    catch (Exception localException)
    {
      k.add(new e("balise racine (a priori <partie>) non trouvée"));
    }
    return localElement;
  }
  
  private Element a(Element paramElement, String paramString)
  {
    Element localElement = null;
    try
    {
      localElement = paramElement.getChild(paramString);
    }
    catch (Exception localException)
    {
      k.add(new e("balise <" + paramString + "> non trouvée"));
    }
    return localElement;
  }
  
  private String a(Element paramElement, String paramString1, String paramString2)
  {
    String str = null;
    if ((paramElement.getAttributeValue(paramString1) == null) || (paramElement.getAttributeValue(paramString1).isEmpty())) {
      k.add(new e(paramString2));
    } else {
      str = new String(paramElement.getAttributeValue(paramString1));
    }
    return str;
  }
  
  private Integer a(String paramString1, String paramString2, int paramInt1, int paramInt2)
  {
    paramInt1 = null;
    if ((paramString1 == null) || (paramString1.isEmpty())) {
      k.add(new e(paramString2 + " n'est pas spécifié"));
    } else if (!paramString1.matches("[0-9]+")) {
      k.add(new e(paramString2 + " (" + paramString1 + ") n'est pas un entier naturel"));
    } else if (paramString1.length() > 9) {
      k.add(new e(paramString2 + " (" + paramString1 + ") est un entier trop grand"));
    } else if ((Integer.valueOf(paramString1).intValue() <= 0) || (Integer.valueOf(paramString1).intValue() > paramInt2)) {
      k.add(new e(paramString2 + " (" + paramString1 + ") doit être compris entre 1" + " et " + paramInt2));
    } else {
      paramInt1 = new Integer(paramString1);
    }
    return paramInt1;
  }
  
  public final a.d a()
  {
    return d;
  }
  
  public final int b()
  {
    return e.intValue();
  }
  
  public final int c()
  {
    return f.intValue();
  }
  
  public final String d()
  {
    return h;
  }
  
  public final List e()
  {
    return i;
  }
  
  public final String f()
  {
    if (d == null) {
      return null;
    }
    return d.a();
  }
  
  public final String g()
  {
    if (d == null) {
      return null;
    }
    return d.c();
  }
  
  public final d.c.d h()
  {
    if (d == null) {
      return null;
    }
    return d.j();
  }
  
  public final int i()
  {
    return j.intValue();
  }
  
  public String toString()
  {
    String str1 = "";
    Iterator localIterator = i.iterator();
    while (localIterator.hasNext())
    {
      String str2 = (String)localIterator.next();
      str1 = str1 + "    " + (str2 == null ? " (votre application)" : new StringBuilder(" nom=").append(str2).toString());
    }
    return "Horodate=" + b + System.lineSeparator() + "Fichier=" + c + System.lineSeparator() + "<partie>" + System.lineSeparator() + "  <equipe>" + " courriel=" + d.a() + " systeme=" + d.j() + System.lineSeparator() + "    " + " commande=" + d.c() + System.lineSeparator() + "  <cave> nombre_lignes=" + e + " nombre_colonnes=" + f + " casiers=" + h + " (nombre_salles=" + g + ")" + System.lineSeparator() + "  <manutentionnaires>" + System.lineSeparator() + str1 + System.lineSeparator() + "  <tours> nombre_tours_desire=" + j + System.lineSeparator() + e.a(k);
  }
}
