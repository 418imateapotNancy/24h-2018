package b.a;

import java.util.Iterator;
import java.util.List;

public final class e
{
  private String a;
  
  public e(String paramString)
  {
    a = paramString;
  }
  
  public final String toString()
  {
    return a;
  }
  
  public static String a(List paramList)
  {
    String str;
    if (paramList.isEmpty())
    {
      str = "";
    }
    else
    {
      str = "Erreur(s) rencontrée(s) lors de la lecture d'une partie décrite dans un fichier XML au cours de la période de développement : " + System.lineSeparator();
      paramList = paramList.iterator();
      while (paramList.hasNext())
      {
        e localE = (e)paramList.next();
        str = str + " - " + localE + System.lineSeparator();
      }
    }
    return str;
  }
}
