package b.a;

import a.a;
import a.c;
import a.f;
import a.k;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class g
  extends f
{
  public g() {}
  
  public g(k paramK, a.b paramB, c paramC, Point paramPoint)
  {
    super(paramK, paramB, paramC, paramPoint);
  }
  
  public final String e()
  {
    return "« intelligence artificielle » " + b.c.a();
  }
  
  public final String f()
  {
    return "« intelligence artificielle » " + b.c.b();
  }
  
  public final a o()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = a.e().iterator();
    while (localIterator.hasNext())
    {
      a localA = (a)localIterator.next();
      if (a.a(b, localA.d())) {
        localArrayList.add(localA);
      }
    }
    if ((z()) && (a.b(b))) {
      localArrayList.add(a.e);
    }
    if ((A()) && (a.d(b))) {
      localArrayList.add(a.f);
    }
    return a(localArrayList);
  }
}
