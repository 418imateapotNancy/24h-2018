package d.b;

import a.j;
import d.a;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

public class c
  extends a
{
  private static final Logger a = Logger.getLogger(c.class.getName());
  private ServerSocket b = null;
  
  public c()
  {
    j.a(a);
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    a.info("Creation du socket serveur");
    b = new ServerSocket(paramInt1);
    b.setSoTimeout(3000);
  }
  
  public final Socket a(int paramInt)
  {
    a.info("Serveur en attente de connexion (accept)");
    paramInt = b.accept();
    a.info("Client connecte !");
    paramInt.setSoTimeout(3000);
    OutputStream localOutputStream = paramInt.getOutputStream();
    InputStream localInputStream;
    if (((localInputStream = paramInt.getInputStream()) == null) || (localOutputStream == null))
    {
      c(paramInt);
      paramInt = "Serveur : problème de flux d'entrée ou de sortie du client";
      a.severe(paramInt);
      throw new RuntimeException(paramInt);
    }
    return paramInt;
  }
  
  public final void c(Socket paramSocket)
  {
    if ((paramSocket != null) && (!paramSocket.isClosed())) {
      try
      {
        if ((!paramSocket.isClosed()) && ((localObject = paramSocket.getInputStream()) != null)) {
          ((InputStream)localObject).close();
        }
        if ((!paramSocket.isClosed()) && ((localObject = paramSocket.getOutputStream()) != null)) {
          ((OutputStream)localObject).close();
        }
        paramSocket.close();
        return;
      }
      catch (Exception localException)
      {
        Object localObject;
        a(localObject = localException, "deconnexion", "Serveur : problème lors de la déconnexion du client");
      }
    }
  }
  
  public final void a()
  {
    a.info("Arrêt du serveur TCP");
    if (b != null) {
      try
      {
        b.close();
        return;
      }
      catch (Exception localException2)
      {
        Exception localException1;
        a(localException1 = localException2, "arret", "Erreur à la fermeture du socket serveur");
      }
    }
  }
}
