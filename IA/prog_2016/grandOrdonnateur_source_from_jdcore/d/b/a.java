package d.b;

import a.g;
import a.j;
import a.k;
import a.m;
import a.o;
import java.awt.Point;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;

public class a
{
  private static final Logger a = Logger.getLogger(a.class.getName());
  private final String b;
  private final int c;
  private final String d;
  private final int e;
  private d.c.a f;
  private d.c.a g;
  private c h;
  
  public a(String paramString1, int paramInt1, String paramString2, int paramInt2)
  {
    j.a(a);
    a.info("Démarrage à " + new Date());
    b = paramString1;
    c = paramInt1;
    d = null;
    e = -1;
    f = null;
    g = null;
    h = null;
  }
  
  private void a(k paramK, long paramLong)
  {
    h = new c();
    h.a(c, 3000);
    Object localObject1 = paramK;
    a localA = this;
    localObject1 = ((k)localObject1).c().iterator();
    while (((Iterator)localObject1).hasNext())
    {
      Object localObject2;
      if (!(localObject2 = (g)((Iterator)localObject1).next()).n())
      {
        if ((localObject2 = (a.d)localObject2).j() == d.c.d.a)
        {
          if (f == null) {
            f = new d.c.c(b, c);
          }
          ((a.d)localObject2).a(f);
        }
        else if (((a.d)localObject2).j() == d.c.d.b)
        {
          if (g == null) {
            g = new d.c.b(d, e);
          }
          ((a.d)localObject2).a(g);
        }
        else
        {
          localObject3 = "Systeme d'exploitation incorrect";
          a.severe((String)localObject3);
          throw new IllegalArgumentException((String)localObject3);
        }
        ((a.d)localObject2).a(h);
        ((a.d)localObject2).d().a((a.d)localObject2);
        a.info("En attente de connexion d'un manutentionnaire");
        if ((localObject3 = h.a(3000)) == null)
        {
          h.a();
          break;
        }
        ((a.d)localObject2).a((Socket)localObject3);
        String str = h.b((Socket)localObject3);
        a.info("Nom d'équipe reçu : " + str);
        if (str == null)
        {
          h.a();
          break;
        }
        if (!a.d.e(str)) {
          throw new IllegalArgumentException("Nom d'équipe invalide : " + str);
        }
        ((a.d)localObject2).a(str);
        Object localObject3 = h.b((Socket)localObject3);
        a.info("Nom de l'IUT reçu : " + (String)localObject3);
        if (localObject3 == null)
        {
          h.a();
          break;
        }
        ((a.d)localObject2).b((String)localObject3);
      }
    }
    paramK.a(paramLong);
    a(paramK);
    paramK.f();
    b(paramK);
    h.a();
  }
  
  private void a(k paramK)
  {
    Object localObject1 = (localObject1 = paramK.c()).listIterator(((List)localObject1).size());
    while (((ListIterator)localObject1).hasPrevious())
    {
      Object localObject2;
      if (!(localObject2 = (g)((ListIterator)localObject1).previous()).n())
      {
        a.d localD = (a.d)localObject2;
        a.info("Envoi du nombre de lignes");
        h.a(bax, localD.k());
        a.info("Envoi du nombre de colonnes");
        h.a(bay, localD.k());
        a.info("Envoi des casiers de la cave");
        h.a(paramK.b().b(), localD.k());
        a.info("Envoi du nombre de manutentionnaires");
        h.a(paramK.c().size(), localD.k());
        a.info("Envoi de l'ordre parmi les manutentionnaires");
        h.a((localObject2 = ((g)localObject2).u()).ordinal() + 1, localD.k());
      }
    }
  }
  
  private void b(k paramK)
  {
    a.info("Arrêt à " + new Date());
    paramK = paramK.c().iterator();
    while (paramK.hasNext())
    {
      Object localObject;
      if (!(localObject = (g)paramK.next()).n())
      {
        localObject = (a.d)localObject;
        h.c(((a.d)localObject).k());
        a.info("destruction du client de l'équipe " + ((a.d)localObject).g());
        ((a.d)localObject).d().a(((a.d)localObject).h());
      }
    }
  }
  
  private static String[] a(m paramM)
  {
    String[] arrayOfString = null;
    switch (b.a[paramM.ordinal()])
    {
    case 1: 
      arrayOfString = new String[] { "chemin_vers/partie.xml", "chemin_vers_binaires_equipe", "dureeMaxDesiree", "dureeTempoActionAffichage", "numEquipe" };
      break;
    case 2: 
      arrayOfString = new String[] { "dureeMaxDesiree", "dureeTempoActionAffichage" };
      break;
    case 3: 
      arrayOfString = new String[] { "numEquipe", "chemin_vers/partie.xml", "chemin_vers_binaires_equipe", "dureeMaxDesiree", "nbToursDesire" };
      break;
    case 4: 
      arrayOfString = new String[] { "numEquipe1", "chemin_vers_equipe1/partie.xml", "chemin_vers_binaires_equipe1", "numEquipe2", "chemin_vers_equipe2/partie.xml", "chemin_vers_binaires_equipe2", "numEquipe3", "chemin_vers_equipe3/partie.xml", "chemin_vers_binaires_equipe3", "dureeMaxDesiree", "nbToursDesire" };
    }
    return arrayOfString;
  }
  
  public final void a(String[] paramArrayOfString1, String[] paramArrayOfString2, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    paramBoolean1 = a(m.c);
    paramBoolean3 = paramBoolean1;
    Object localObject = (String[])Arrays.copyOf(paramBoolean2 = paramArrayOfString2, paramBoolean2.length + paramBoolean3.length);
    System.arraycopy(paramBoolean3, 0, localObject, paramBoolean2.length, paramBoolean3.length);
    paramArrayOfString2 = (String[])localObject;
    if (paramArrayOfString1.length != 2 + paramBoolean1.length)
    {
      paramBoolean2 = paramArrayOfString2;
      (paramBoolean3 = new StringBuffer()).append("arguments : ");
      paramBoolean1 = (localObject = paramBoolean2).length;
      paramBoolean2 = false;
      String str = localObject[paramBoolean2];
      paramBoolean3.append(str).append(" ");
      System.out.println(paramBoolean3.toString());
      System.exit(1);
    }
    paramBoolean1 = o.b("dureeMaxDesiree", paramArrayOfString2, paramArrayOfString1);
    paramArrayOfString1 = o.c("dureeTempoActionAffichage", paramArrayOfString2, paramArrayOfString1);
    try
    {
      a.info("----- nom de fichier = \"partie.xml\" -----");
      paramArrayOfString2 = new b.a.d("partie.xml");
      a.info(paramArrayOfString2.toString());
      (paramBoolean2 = paramArrayOfString2.a()).a(paramArrayOfString2);
      paramBoolean2.a(d.c.d.a);
      (paramBoolean1 = new k(paramArrayOfString2.i(), paramBoolean1, true, false, false, m.c)).a(new a.b(paramArrayOfString2));
      paramBoolean2.a(1);
      paramBoolean2.c("executable");
      paramBoolean1.a(paramArrayOfString2.e(), Arrays.asList(new a.d[] { paramBoolean2 }));
      a(paramBoolean1, paramArrayOfString1.longValue());
      return;
    }
    catch (Exception paramArrayOfString2)
    {
      paramArrayOfString2.getMessage();
      o.a(a, paramArrayOfString2);
    }
  }
  
  public final void a(String[] paramArrayOfString1, String[] paramArrayOfString2, m paramM)
  {
    a.info("Paramètres passés en ligne de commande :");
    paramM = a(paramM);
    for (int i = 0; i < paramArrayOfString1.length; i++)
    {
      (localObject = new StringBuffer()).append("args[" + i + "]=" + paramArrayOfString1[i]);
      if (i < 2)
      {
        ((StringBuffer)localObject).append(" (");
        ((StringBuffer)localObject).append(paramArrayOfString2[i]);
        ((StringBuffer)localObject).append(")");
      }
      else if (i < 2 + paramM.length)
      {
        ((StringBuffer)localObject).append(" (");
        ((StringBuffer)localObject).append(paramM[(i - 2)]);
        ((StringBuffer)localObject).append(")");
      }
      a.info(((StringBuffer)localObject).toString());
    }
    a.info("Paramètres du fichier 24h.properties :");
    Properties localProperties;
    Object localObject = (localProperties = j.a()).stringPropertyNames().iterator();
    while (((Iterator)localObject).hasNext())
    {
      paramArrayOfString1 = (String)((Iterator)localObject).next();
      (paramArrayOfString2 = new StringBuffer()).append(paramArrayOfString1).append(" = ");
      paramArrayOfString2.append(localProperties.get(paramArrayOfString1));
      a.info(paramArrayOfString2.toString());
    }
  }
}
