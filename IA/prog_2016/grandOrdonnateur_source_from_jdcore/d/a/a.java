package d.a;

import a.j;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class a
  extends d.a
{
  private static final Logger a = Logger.getLogger(a.class.getName());
  private final String b;
  private final int c;
  private Socket d;
  
  public a(String paramString, int paramInt)
  {
    j.a(a);
    b = paramString;
    c = paramInt;
  }
  
  public final void a()
  {
    a.info("Client : connexion sur " + b + ":" + c + " à " + new Date());
    d = new Socket(b, c);
    a.log(Level.INFO, "connecté au serveur");
    a.info("test du flux en entrée");
    d.getInputStream();
    a.info("test du flux en sortie");
    d.getOutputStream();
  }
  
  public final void b()
  {
    a.info("Client : demande de déconnexion à " + new Date());
    if ((d != null) && ((localObject = d.getInputStream()) != null)) {
      ((InputStream)localObject).close();
    }
    Object localObject = null;
    if (d != null) {
      localObject = d.getOutputStream();
    }
    if (localObject != null) {
      ((OutputStream)localObject).close();
    }
    if (d != null) {
      d.close();
    }
    a.info("Client : arrêt à " + new Date());
  }
  
  public final void a(int paramInt)
  {
    a(paramInt, d);
  }
  
  public final void a(char paramChar)
  {
    a(paramChar, d);
  }
  
  public final void a(String paramString)
  {
    a(paramString, d);
  }
}
