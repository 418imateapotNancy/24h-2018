package d.c;

import a.d;
import a.j;
import a.o;
import java.util.logging.Logger;

public class b
  extends a
{
  private static final Logger c = Logger.getLogger(b.class.getName());
  private final d.a.a d;
  
  public b(String paramString, int paramInt)
  {
    j.a(c);
    d = new d.a.a(paramString, paramInt);
    d.a();
  }
  
  public final void a(d paramD)
  {
    if (paramD == null)
    {
      c.severe("Equipe null : abandon");
      a();
    }
    d.a('L');
    int i = paramD.h();
    d.a(i);
    String str;
    c.severe("La chaîne \"" + str + "\" doit être envoyée au relai distant mais est null. Abandon.");
    c.severe("La chaîne \"" + str + "\" doit être envoyée au relai distant mais contient le caractère réservé " + '~' + ". Abandon.");
    if ((str.indexOf('~') >= 0 ? 0 : (str = paramD = paramD.c()) == null ? 0 : 1) == 0) {
      a();
    }
    d.a(paramD);
  }
  
  public final void a(int paramInt)
  {
    try
    {
      d.a('T');
      d.a(paramInt);
      return;
    }
    catch (Exception paramInt)
    {
      getClass().getName();
      paramInt.getMessage();
      o.a(c, paramInt);
    }
  }
  
  private void a()
  {
    d.a('Q');
    d.b();
  }
}
