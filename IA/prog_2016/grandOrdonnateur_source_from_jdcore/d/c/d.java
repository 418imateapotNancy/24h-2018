package d.c;

public enum d
{
  private final String c;
  
  public static d[] a()
  {
    return (d[])d.clone();
  }
  
  private d(String paramString)
  {
    c = paramString;
  }
  
  public final String toString()
  {
    return c;
  }
  
  private boolean b(String paramString)
  {
    if (paramString == null) {
      return false;
    }
    return c.equalsIgnoreCase(paramString);
  }
  
  public static d a(String paramString)
  {
    d localD = null;
    if (a.b(paramString)) {
      localD = a;
    } else if (b.b(paramString)) {
      localD = b;
    }
    return localD;
  }
}
