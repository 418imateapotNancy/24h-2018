package d.c;

import a.d;
import a.j;
import a.o;
import java.io.File;
import java.util.Arrays;
import java.util.logging.Logger;

public class c
  extends a
{
  private static final Logger c = Logger.getLogger(c.class.getName());
  private Process[] d;
  private ProcessBuilder e;
  private String f = null;
  
  private c(String paramString, int paramInt, boolean paramBoolean)
  {
    j.a(c, f);
    d = new Process[31];
    for (paramBoolean = false; paramBoolean < true; paramBoolean++) {
      d[paramBoolean] = null;
    }
    e = new ProcessBuilder(new String[0]);
    a = paramString;
    b = paramInt;
  }
  
  public c(String paramString, int paramInt)
  {
    this(paramString, paramInt, false);
  }
  
  public final void a(d paramD)
  {
    if (paramD == null)
    {
      c.severe("Lancement du joueur avec une equipe null : abandon");
      return;
    }
    if ((localObject1 = paramD.c()) == null)
    {
      c.severe("Lancement du joueur avec une commande null : abandon");
      return;
    }
    int i;
    if (((i = paramD.h()) < 0) || (i > 31))
    {
      c.severe("Numero de l'équipe invalide (" + i + ") : abandon");
      return;
    }
    if (d[(i - 1)] != null)
    {
      c.severe("Lancement du joueur pour une equipe ayant deja un processus actif : abandon");
      return;
    }
    Object localObject1 = a((String)localObject1, a, b);
    e.command((String[])localObject1);
    Object localObject2 = paramD.m();
    File localFile = null;
    c.info("Fichier journal de l'équipe : " + (String)localObject2);
    try
    {
      (localFile = new File((String)localObject2)).createNewFile();
    }
    catch (Exception localException)
    {
      localObject2 = "Impossible de créer le fichier " + (String)localObject2;
      localObject2 = "lancerJoueur";
      localObject3 = localException;
      localObject2 = this;
      localObject2.getClass().getName();
      o.a(c, (Exception)localObject3);
    }
    if (localFile != null)
    {
      e.redirectError(localFile);
      e.redirectOutput(localFile);
    }
    e.directory(new File(paramD.l()));
    c.info("Lancement du joueur via la commande : " + Arrays.toString((Object[])localObject1));
    Object localObject3 = e.start();
    d[(i - 1)] = localObject3;
  }
  
  public final void a(int paramInt)
  {
    c.info("Demande pour tuer le joueur de l'équipe " + paramInt);
    Process localProcess;
    if ((localProcess = d[(paramInt - 1)]) == null)
    {
      c.info("Pas de processus référencé pour cette équipe");
      return;
    }
    d[(paramInt - 1)] = null;
    localProcess.destroy();
  }
}
