package d;

import a.o;
import d.b.c;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class a
{
  private static final Logger a = Logger.getLogger(c.class.getName());
  
  public a() {}
  
  private void a(int paramInt, Socket paramSocket, boolean paramBoolean)
  {
    if ((paramSocket = paramSocket) == null) {
      throw new IllegalArgumentException("clientSocket null");
    }
    if ((paramSocket = paramSocket.getOutputStream()) == null)
    {
      a.warning("Demande d'envoi d'un entier vers un flux null. Abandon.");
      return;
    }
    if (paramBoolean) {
      a.info("Envoi de l'entier : " + paramInt);
    }
    paramSocket.write(paramInt);
    paramSocket.flush();
  }
  
  public final void a(int paramInt, Socket paramSocket)
  {
    a(paramInt, paramSocket, true);
  }
  
  public final void a(char paramChar, Socket paramSocket)
  {
    Socket localSocket = paramSocket;
    paramSocket = paramChar;
    paramChar = this;
    a(paramSocket, localSocket, true);
  }
  
  public final void a(String paramString, Socket paramSocket)
  {
    if (paramString == null) {
      throw new IllegalArgumentException("Demande d'envoi d'une chaine null. Abandon.");
    }
    String str;
    if (!((str = paramString) == null ? false : Pattern.matches("[\\x20-\\x7d]*", str)))
    {
      str = "Demande (non réalisée) d'envoi de la chaine suivante, contenant des caractères spéciaux : " + paramString;
      a.severe(str);
      throw new IllegalArgumentException(str);
    }
    a.info("Envoi de la chaine : " + paramString);
    for (int i = 0; i < paramString.length(); i++)
    {
      int j = paramString.charAt(i);
      a(j, paramSocket, false);
    }
    a(126, paramSocket, false);
  }
  
  private int a(Socket paramSocket, boolean paramBoolean)
  {
    if ((paramSocket = paramSocket) == null) {
      throw new IllegalArgumentException("clientSocket null");
    }
    if ((paramSocket = paramSocket.getInputStream()) == null) {
      throw new IllegalArgumentException("fluxEntree null");
    }
    if (paramBoolean) {
      a.info("En attente de réception d'un entier");
    }
    paramSocket = paramSocket.read();
    if (paramBoolean) {
      a.info("Entier recu : " + paramSocket);
    }
    return paramSocket;
  }
  
  public final int a(Socket paramSocket)
  {
    return a(paramSocket, true);
  }
  
  public final String b(Socket paramSocket)
  {
    a.info("receptionChaine");
    StringBuffer localStringBuffer = new StringBuffer();
    int i = 0;
    int j = 1;
    while ((j != 0) && (i == 0))
    {
      char c;
      j = (c = (char)a(paramSocket, false)) != 65535 ? 1 : 0;
      i = c == '~' ? 1 : 0;
      if ((j != 0) && (i == 0)) {
        localStringBuffer.append(c);
      }
    }
    a.info("receptionChaine : chaine = " + localStringBuffer.toString());
    if (j != 0) {
      return localStringBuffer.toString();
    }
    return null;
  }
  
  protected static void a(Exception paramException, String paramString1, String paramString2)
  {
    o.a(a, paramException);
  }
}
