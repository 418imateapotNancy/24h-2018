package a;

import java.awt.Point;
import java.util.List;
import java.util.Random;

public abstract class f
  extends g
{
  public f() {}
  
  public f(k paramK, b paramB, c paramC, Point paramPoint)
  {
    super(paramK, paramB, paramC, paramPoint);
  }
  
  public abstract String e();
  
  public String f()
  {
    return e();
  }
  
  public final boolean n()
  {
    return true;
  }
  
  public static a a(List paramList)
  {
    if (paramList.isEmpty()) {
      throw new NullPointerException("Aucune action n'est possible.");
    }
    return (a)paramList.get(new Random().nextInt(paramList.size()));
  }
}
