package a;

import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

public class d
  extends g
{
  private static final Logger d = Logger.getLogger(d.class.getName());
  private String e = null;
  private int f = -1;
  private String g = null;
  private String h;
  private ArrayList i = new ArrayList();
  private String j;
  private String k;
  private d.c.d l;
  private d.c.a m;
  private d.b.c n;
  private Socket o;
  
  public d(String paramString1, int paramInt, String paramString2, String paramString3, String paramString4, d.c.d paramD)
  {
    h = paramString3;
    j = paramString4;
    l = paramD;
  }
  
  public final String a()
  {
    return h;
  }
  
  public final ArrayList b()
  {
    return i;
  }
  
  public final void a(long paramLong)
  {
    i.add(Long.valueOf(paramLong));
  }
  
  public final String c()
  {
    return j;
  }
  
  public final void a(d.c.a paramA)
  {
    m = paramA;
  }
  
  public final d.c.a d()
  {
    return m;
  }
  
  public final String e()
  {
    d localD = this;
    return e;
  }
  
  public final String f()
  {
    d localD = this;
    localD = this;
    return e + System.lineSeparator() + g;
  }
  
  public final String g()
  {
    return e;
  }
  
  public final void a(String paramString)
  {
    e = paramString;
  }
  
  public final int h()
  {
    return f;
  }
  
  public final void a(int paramInt)
  {
    f = 1;
  }
  
  public final String i()
  {
    return g;
  }
  
  public final void b(String paramString)
  {
    g = paramString;
  }
  
  public final d.c.d j()
  {
    return l;
  }
  
  public final void a(d.c.d paramD)
  {
    l = paramD;
  }
  
  public final void a(d.b.c paramC)
  {
    n = paramC;
  }
  
  public final void a(Socket paramSocket)
  {
    o = paramSocket;
  }
  
  public final Socket k()
  {
    return o;
  }
  
  public final void c(String paramString)
  {
    k = paramString;
  }
  
  public final String l()
  {
    return k;
  }
  
  public final void a(b.a.d paramD)
  {
    h = paramD.f();
    j = paramD.g();
    l = paramD.h();
  }
  
  public final String m()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    if (l != null)
    {
      localObject = null;
      switch (e.a[l.ordinal()])
      {
      case 1: 
        localObject = j.b();
        break;
      case 2: 
        localObject = "Z:";
      }
      if (localObject != null) {
        localStringBuffer.append((String)localObject);
      }
    }
    if (localStringBuffer.length() == 0) {
      return "sortie.log";
    }
    localStringBuffer.append(System.getProperty("file.separator"));
    localStringBuffer.append("equipe");
    Object localObject = this;
    localStringBuffer.append((f < 10 ? "0" : "") + f);
    localStringBuffer.append(System.getProperty("file.separator"));
    localStringBuffer.append("prog");
    localStringBuffer.append(System.getProperty("file.separator"));
    localStringBuffer.append("sortie.log");
    return localStringBuffer.toString();
  }
  
  public final boolean n()
  {
    return false;
  }
  
  public final a o()
  {
    Socket localSocket = o;
    d.b.c localC;
    char c = (char)(localC = n).a(localSocket);
    d.info("Serveur : action recue : " + c);
    return a.a(c);
  }
  
  public final void d(String paramString)
  {
    d.info("Envoi de la séquence d'actions jouées par un autre manu. (serveur -> client) :" + paramString);
    n.a(paramString, o);
  }
  
  public static boolean e(String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    (localObject1 = new ArrayList()).add(b.a.a.class);
    ((List)localObject1).add(b.a.c.class);
    ((List)localObject1).add(b.a.g.class);
    ((List)localObject1).add(c.a.a.class);
    Object localObject1 = ((List)localObject1).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      Object localObject2 = (Class)((Iterator)localObject1).next();
      String str = null;
      try
      {
        str = ((f)((Class)localObject2).newInstance()).e();
      }
      catch (InstantiationException|IllegalAccessException localInstantiationException)
      {
        (localObject2 = localInstantiationException).printStackTrace();
      }
      localArrayList.add(str);
      localArrayList.add(str.substring(30));
    }
    return !localArrayList.contains(paramString);
  }
}
