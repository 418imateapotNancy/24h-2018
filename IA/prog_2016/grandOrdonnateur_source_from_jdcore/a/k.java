package a;

import java.awt.Point;
import java.net.SocketTimeoutException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

public class k
{
  private static final Logger a = Logger.getLogger(k.class.getName());
  private b b;
  private List c;
  private int d;
  private Date e;
  private Date f;
  private Date g;
  private String h = "";
  private String i = "";
  private a.a.c j;
  private boolean k;
  private boolean l;
  private boolean m;
  private final m n;
  
  public k(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, m paramM)
  {
    boolean bool = paramBoolean3;
    paramBoolean3 = paramBoolean2;
    paramBoolean2 = paramBoolean1;
    paramBoolean1 = paramInt2;
    paramInt2 = paramInt1;
    paramInt1 = this;
    d = paramInt2;
    e = new Date();
    g = new Date(e.getTime() + paramBoolean1 * true * 1000);
    k = paramBoolean2;
    if (k) {
      j.a(a);
    }
    l = paramBoolean3;
    m = bool;
    n = paramM;
    if (paramM == null) {
      throw new IllegalArgumentException("Phase null");
    }
  }
  
  public final void a(long paramLong)
  {
    j = (paramLong >= 0L ? new a.a.c(this, paramLong) : null);
  }
  
  public final void a(List paramList1, List paramList2)
  {
    Iterator localIterator = null;
    if ((paramList2 != null) && (!paramList2.isEmpty())) {
      localIterator = paramList2.iterator();
    }
    c = new ArrayList();
    c localC = c.d();
    paramList1 = paramList1.iterator();
    while (paramList1.hasNext())
    {
      Object localObject = (String)paramList1.next();
      Point localPoint = new Point(b.c());
      if (localObject == null)
      {
        if (paramList2 == null) {
          throw new IllegalArgumentException("Aucune équipe n'a été définie.");
        }
        if (localIterator.hasNext())
        {
          (localObject = (d)localIterator.next()).a(this);
          ((d)localObject).a(b);
          ((d)localObject).a(localC);
          ((d)localObject).a(localPoint);
          c.add(localObject);
        }
        else
        {
          throw new ArrayIndexOutOfBoundsException("Il n'y a pas assez d'équipes de définies.");
        }
      }
      else
      {
        switch (l.a[n.ordinal()])
        {
        case 1: 
          if (((String)localObject).equals(b.a.b.a.a())) {
            c.add(new b.a.a(this, b, localC, localPoint));
          } else if (((String)localObject).equals(b.a.b.b.a())) {
            c.add(new b.a.c(this, b, localC, localPoint));
          } else if (((String)localObject).equals(b.a.b.c.a())) {
            c.add(new b.a.g(this, b, localC, localPoint));
          } else {
            throw new IllegalArgumentException("Seules les IAs tacheron, olibrius et bradype sont disponibles en phase de développement.");
          }
          break;
        case 2: 
          if (((String)localObject).equals(b.a.b.b.a())) {
            c.add(new b.a.c(this, b, localC, localPoint));
          } else {
            throw new IllegalArgumentException("Seule l'IA olibrius est disponible en phase de développement en local.");
          }
          break;
        case 3: 
          if (((String)localObject).equals("GoGol")) {
            c.add(new c.a.a(this, b, localC, localPoint));
          } else {
            throw new IllegalArgumentException("Seule l'IA Gogol est disponible en phase de classement.");
          }
          break;
        case 4: 
          throw new IllegalArgumentException("Pas de recours aux intelligences artificielles en finale.");
        case 5: 
          if (((String)localObject).equals(b.a.b.a.a())) {
            c.add(new b.a.a(this, b, localC, localPoint));
          } else if (((String)localObject).equals(b.a.b.b.a())) {
            c.add(new b.a.c(this, b, localC, localPoint));
          } else if (((String)localObject).equals(b.a.b.c.a())) {
            c.add(new b.a.g(this, b, localC, localPoint));
          } else if (((String)localObject).equals("GoGol")) {
            c.add(new c.a.a(this, b, localC, localPoint));
          } else {
            throw new IllegalArgumentException("Seules les IAs tacheron, olibrius, bradype et gogol sont disponibles en phase de développement, et pas " + (String)localObject);
          }
          break;
        }
      }
      localC = localC.e();
    }
  }
  
  public String toString()
  {
    String str = "Partie : phase=" + n + ", horodateDebut=" + e + ", horodateFin=" + f + ", historique=" + i + ", penalites=" + h + System.lineSeparator();
    Iterator localIterator = c.iterator();
    while (localIterator.hasNext())
    {
      g localG = (g)localIterator.next();
      str = str + "  " + localG.toString() + System.lineSeparator();
    }
    return str;
  }
  
  public final a.a.c a()
  {
    return j;
  }
  
  public final b b()
  {
    return b;
  }
  
  public final void a(b paramB)
  {
    b = paramB;
  }
  
  public final List c()
  {
    return c;
  }
  
  public final String d()
  {
    return h;
  }
  
  public final String e()
  {
    return i;
  }
  
  private Double g()
  {
    return Double.valueOf((f.getTime() - e.getTime()) / 1000.0D);
  }
  
  public final void f()
  {
    int i1 = 0;
    g localG = (g)c.get(0);
    int i2 = 7;
    HashMap localHashMap = new HashMap();
    if (j != null)
    {
      localObject = j.c().b().iterator();
      while (((Iterator)localObject).hasNext())
      {
        a.a.a localA = (a.a.a)((Iterator)localObject).next();
        localHashMap.put(localA.a(), localA);
      }
    }
    Object localObject = new Date();
    int i3 = 0;
    int i4 = 1;
    int i6 = 0;
    StringBuffer localStringBuffer = new StringBuffer();
    HashSet localHashSet = new HashSet();
    for (;;)
    {
      int i8 = i6;
      int i7 = i4;
      k localK = this;
      if (((d == 0) || ((g.before(new Date())) && (i7 != 0)) || (b.e() == 0) || (i8 == c.size()) ? 1 : 0) != 0) {
        break;
      }
      int i5 = 0;
      i7 = 0;
      i8 = 0;
      i3 = 0;
      a localA1 = null;
      if (localHashSet.contains(localG)) {
        localA1 = a.g;
      } else {
        try
        {
          localA1 = localG.o();
        }
        catch (SocketTimeoutException localSocketTimeoutException)
        {
          i8 = 1;
        }
      }
      if (i8 != 0)
      {
        localA1 = a.g;
        localHashSet.add(localG);
        if (!localG.n())
        {
          d localD;
          (localD = (d)localG).d().a(localD.h());
        }
      }
      if ((i2 == 7) && ((localA1 == null) || ((localA1 != null) && (!localA1.equals(a.g)))))
      {
        localG.x();
        i6 = 0;
      }
      localStringBuffer.append(localA1 == null ? a.c() : localA1.b());
      if (localA1 == null)
      {
        h += '^';
        i += a.c();
        throw new NullPointerException("L'action n'est pas définie.");
      }
      if ((a.e().contains(localA1)) || (a.f().contains(localA1)))
      {
        h += (localG.a(localA1) ? '.' : '^');
        i += localA1.b();
        if (i2 == 1) {
          i7 = 1;
        } else {
          i2--;
        }
      }
      else if (localA1.equals(a.g))
      {
        h += (i8 != 0 ? '^' : '.');
        i += localA1.b();
        i7 = 1;
        if (i2 == 7) {
          i6++;
        }
      }
      else
      {
        h += '^';
        i += a.c();
        throw new NullPointerException("L'action n'est pas définie.");
      }
      if (i7 != 0)
      {
        h += ' ';
        i += ' ';
        long l1;
        if ((l1 = new Date().getTime() - ((Date)localObject).getTime()) > 3000L) {
          localG.y();
        }
        if (!localG.n())
        {
          ((d)localG).a(l1);
          i3 = 1;
        }
        a(localStringBuffer.toString(), localG, localHashSet);
        localStringBuffer = new StringBuffer();
        if (i1 == c.size() - 1)
        {
          d -= 1;
          i1 = 0;
          i5 = 1;
        }
        else
        {
          i1++;
        }
        if (j != null) {
          ((a.a.a)localHashMap.get(localG)).j();
        }
        localG = (g)c.get(i1);
        i2 = 7;
        localObject = new Date();
      }
      else if (j != null)
      {
        ((a.a.a)localHashMap.get(localG)).j();
      }
      if (j != null) {
        j.c().a();
      }
    }
    if ((i3 == 0) && (!localG.n())) {
      ((d)localG).a(new Date().getTime() - ((Date)localObject).getTime());
    }
    h();
  }
  
  private void a(String paramString, g paramG, Set paramSet)
  {
    Iterator localIterator = c.iterator();
    while (localIterator.hasNext())
    {
      g localG;
      if (((localG = (g)localIterator.next()) != null) && (!localG.equals(paramG)) && (!paramSet.contains(localG))) {
        localG.d(paramString);
      }
    }
  }
  
  private void h()
  {
    f = new Date();
    if ((i.length() > 0) && (i.charAt(i.length() - 1) != ' '))
    {
      i += ' ';
      h += ' ';
    }
    String str1 = j();
    if (k) {
      a.info(str1);
    }
    if (l) {
      i();
    }
    if (m)
    {
      Iterator localIterator = c.iterator();
      while (localIterator.hasNext())
      {
        Object localObject;
        if (!(localObject = (g)localIterator.next()).n())
        {
          String str2 = str1;
          d localD = (d)localObject;
          localObject = this;
          o.a(localD.a(), "[24h IUT Info Bordeaux 2016 / épreuve développement / période développement] résultats partie jouée selon \"partie.xml\"", "Bonjour " + localD.g() + "," + System.lineSeparator() + System.lineSeparator() + "Voici les résultats de la partie jouée à partir de votre fichier \"partie.xml\" lors de la période de développement de cette épreuve de développement de l'édition 2016 (Bordeaux) des 24h des IUT Informatique." + System.lineSeparator() + System.lineSeparator() + str2 + a(localD) + System.lineSeparator());
        }
      }
    }
  }
  
  private void i()
  {
    try
    {
      Class.forName("com.mysql.jdbc.Driver");
      try
      {
        Connection localConnection = DriverManager.getConnection("jdbc:mysql://localhost/24h?verifyServerCertificate=false&useSSL=false&requireSSL=false", "24h", "24hIUTInfo");
        Object localObject1 = "INSERT INTO Partie (Id, Phase, Horodate_Debut, Horodate_Fin, Duree, Nb_Lig_Cave, Nb_Col_Cave, Casiers_Cave, Lig_Salle_Escalier_Cave, Col_Salle_Escalier_Cave, Historique, Penalites) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        localObject1 = localConnection.prepareStatement((String)localObject1);
        Object localObject2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        ((PreparedStatement)localObject1).setString(1, n.toString());
        ((PreparedStatement)localObject1).setString(2, ((DateFormat)localObject2).format(e));
        ((PreparedStatement)localObject1).setString(3, ((DateFormat)localObject2).format(f));
        ((PreparedStatement)localObject1).setDouble(4, g().doubleValue());
        ((PreparedStatement)localObject1).setInt(5, b.a().x);
        ((PreparedStatement)localObject1).setInt(6, b.a().y);
        ((PreparedStatement)localObject1).setString(7, b.b());
        ((PreparedStatement)localObject1).setInt(8, b.c().x + 1);
        ((PreparedStatement)localObject1).setInt(9, b.c().y + 1);
        ((PreparedStatement)localObject1).setString(10, i);
        ((PreparedStatement)localObject1).setString(11, h);
        ((PreparedStatement)localObject1).execute();
        int i1 = 1;
        localObject2 = c.iterator();
        while (((Iterator)localObject2).hasNext())
        {
          g localG = (g)((Iterator)localObject2).next();
          Object localObject3 = "INSERT INTO Manutentionnaire (Id_Partie, Ordre_Tour, Numero_Equipe, Nom, IUT_Equipe, Courriel_Equipe, Nb_Tours, Nb_Actions, Nb_Total_Pts_Penalites, Nb_Pts_Depots, Score, Nb_Bouteilles_Dans_Sac, Lig_Salle_Cave, Col_Salle_Cave) VALUES (LAST_INSERT_ID(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
          (localObject3 = localConnection.prepareStatement((String)localObject3)).setInt(1, i1);
          if (localG.n())
          {
            ((PreparedStatement)localObject3).setInt(2, 0);
            ((PreparedStatement)localObject3).setString(3, localG.e());
            ((PreparedStatement)localObject3).setNull(4, 12);
            ((PreparedStatement)localObject3).setNull(5, 12);
          }
          else
          {
            d localD = (d)localG;
            ((PreparedStatement)localObject3).setInt(2, localD.h());
            ((PreparedStatement)localObject3).setString(3, localD.g());
            ((PreparedStatement)localObject3).setString(4, localD.i());
            ((PreparedStatement)localObject3).setString(5, localD.a());
          }
          ((PreparedStatement)localObject3).setInt(6, localG.p());
          ((PreparedStatement)localObject3).setInt(7, localG.q());
          ((PreparedStatement)localObject3).setInt(8, localG.r());
          ((PreparedStatement)localObject3).setInt(9, localG.s());
          ((PreparedStatement)localObject3).setInt(10, localG.t());
          ((PreparedStatement)localObject3).setInt(11, localG.w());
          ((PreparedStatement)localObject3).setInt(12, vx + 1);
          ((PreparedStatement)localObject3).setInt(13, vy + 1);
          ((PreparedStatement)localObject3).execute();
          i1++;
        }
        localConnection.close();
      }
      catch (SQLException localSQLException)
      {
        if (k) {
          a.severe("Connexion impossible à la base de données des 24h (SQLException)." + localSQLException);
        }
        localSQLException.printStackTrace();
      }
      catch (Exception localException2)
      {
        if (k) {
          a.severe("Connexion impossible à la base de données des 24h." + localException2);
        }
        localException2.printStackTrace();
        return;
      }
      return;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      if (k) {
        a.severe("JDBC non utilisable (ClassNotFoundException)." + localClassNotFoundException);
      }
      localClassNotFoundException.printStackTrace();
      return;
    }
    catch (Exception localException1)
    {
      if (k) {
        a.severe("JDBC non utilisable." + localException1);
      }
      localException1.printStackTrace();
    }
  }
  
  private String j()
  {
    Object localObject = new SimpleDateFormat("dd/MM/yyyy HH':'mm':'ss'.'SSS");
    String str = "";
    str = str + "Partie" + System.lineSeparator();
    str = str + "  Début : " + ((DateFormat)localObject).format(e) + System.lineSeparator();
    str = str + "  Fin : " + ((DateFormat)localObject).format(f) + System.lineSeparator();
    str = str + "  Durée : " + g() + System.lineSeparator();
    str = str + "Cave" + System.lineSeparator();
    str = str + "  Nombre de lignes : " + b.a().x + System.lineSeparator();
    str = str + "  Nombre de colonnes : " + b.a().y + System.lineSeparator();
    str = str + "  Casiers : " + b.b() + System.lineSeparator();
    str = str + "  Salle disposant de l'escalier : " + a(b.c().x, b.c().y) + System.lineSeparator();
    str = str + "Manutentionnaires" + System.lineSeparator();
    localObject = new String[] { "Premier", "Deuxième", "Troisième" };
    int i1 = 0;
    Iterator localIterator = c.iterator();
    while (localIterator.hasNext())
    {
      g localG = (g)localIterator.next();
      str = str + "  " + localObject[i1] + " manutentionnaire (" + localG.u().c() + ")";
      if (localG.n())
      {
        str = str + " : " + localG.e() + System.lineSeparator();
      }
      else
      {
        str = str + System.lineSeparator() + "    " + "Équipe" + System.lineSeparator();
        d localD = (d)localG;
        str = str + "      Nom : " + localD.g() + System.lineSeparator();
        str = str + "      IUT : " + localD.i() + System.lineSeparator();
        str = str + "      Courriel : " + localD.a() + System.lineSeparator();
      }
      str = str + "    Nombre de tours : " + localG.p() + System.lineSeparator();
      str = str + "    Nombre d'actions : " + localG.q() + System.lineSeparator();
      str = str + "    Nombre total de points de pénalités : " + localG.r() + System.lineSeparator();
      str = str + "    Nombre de points de dépôts de bouteilles : " + localG.s() + System.lineSeparator();
      str = str + "    Score : " + localG.t() + System.lineSeparator();
      str = str + "    Nombre de bouteilles dans le sac : " + localG.w() + System.lineSeparator();
      str = str + "    Salle : " + a(vx, vy) + System.lineSeparator();
      i1++;
    }
    str = str + "Historique : " + i + System.lineSeparator();
    return str = str + "Pénalités  : " + h + System.lineSeparator();
  }
  
  private static String a(d paramD)
  {
    int i1 = 0;
    String str = "Durées (en millisecondes) de vos tours de jeu successifs :";
    paramD = paramD.b().iterator();
    while (paramD.hasNext())
    {
      long l1;
      if ((l1 = ((Long)paramD.next()).longValue()) > 0L)
      {
        str = str + " " + Long.toString(l1);
        if (l1 > 3000L)
        {
          i1 = 1;
          str = str + ">";
        }
      }
    }
    if (i1 != 0) {
      str = str + System.lineSeparator() + "  >" + " : vous avez dépassé (au moins une fois) la durée maximale autorisée par tour de jeu fixée à 3000" + " millisecondes";
    }
    return str;
  }
  
  private static String a(int paramInt1, int paramInt2)
  {
    return "ligne " + (paramInt1 + 1) + " et colonne " + (paramInt2 + 1);
  }
}
