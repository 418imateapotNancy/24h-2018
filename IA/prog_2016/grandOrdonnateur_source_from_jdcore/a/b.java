package a;

import b.a.d;
import java.awt.Point;

public final class b
{
  private Point a;
  private String b;
  private Point c;
  private int[][] d;
  private int e;
  
  private b(int paramInt1, int paramInt2, String paramString, char paramChar)
  {
    a = new Point(paramInt1, paramInt2);
    b = paramString;
    d = new int[paramInt1][paramInt2];
    e = 0;
    paramChar = '\000';
    for (int i = 0; i < paramInt1; i++) {
      for (int j = 0; j < paramInt2; j++)
      {
        char c1;
        if ((c1 = paramString.charAt(paramChar)) == '@')
        {
          c = new Point(i, j);
          d[i][j] = 0;
        }
        else
        {
          d[i][j] = Character.getNumericValue(c1);
          e += d[i][j];
        }
        paramChar++;
      }
    }
  }
  
  public b(d paramD)
  {
    this(paramD.b(), paramD.c(), paramD.d(), '@');
  }
  
  public final Point a()
  {
    return a;
  }
  
  public final String b()
  {
    return b;
  }
  
  public final Point c()
  {
    return c;
  }
  
  public final int[][] d()
  {
    return d;
  }
  
  public final int a(Point paramPoint)
  {
    if (paramPoint == null) {
      throw new NullPointerException("La salle n'est pas définie.");
    }
    return d[x][y];
  }
  
  public final int e()
  {
    return e;
  }
  
  public final boolean a(Point paramPoint, n paramN)
  {
    if (paramPoint == null) {
      throw new NullPointerException("La salle n'est pas définie.");
    }
    if (paramN == null) {
      throw new NullPointerException("Le point cardinal n'est pas défini.");
    }
    return (e(paramPoint)) && (e(new Point(x + bx, y + by)));
  }
  
  private boolean e(Point paramPoint)
  {
    return (x >= 0) && (x < a.x) && (y >= 0) && (y < a.y);
  }
  
  public final boolean b(Point paramPoint)
  {
    if (paramPoint == null) {
      throw new NullPointerException("La salle n'est pas définie.");
    }
    if (!e(paramPoint)) {
      throw new ArrayIndexOutOfBoundsException("La salle n'est pas dans la cave.");
    }
    return d[x][y] > 0;
  }
  
  public final void c(Point paramPoint)
  {
    if (paramPoint == null) {
      throw new NullPointerException("La salle n'est pas définie.");
    }
    if (!e(paramPoint)) {
      throw new ArrayIndexOutOfBoundsException("La salle n'est pas dans la cave.");
    }
    if (d[x][y] == 0) {
      throw new Exception("Le casier de la salle est vide.");
    }
    d[x][y] -= 1;
    e -= 1;
  }
  
  public final boolean d(Point paramPoint)
  {
    if (paramPoint == null) {
      throw new NullPointerException("La salle n'est pas définie.");
    }
    if (!e(paramPoint)) {
      throw new ArrayIndexOutOfBoundsException("La salle n'est pas dans la cave.");
    }
    return paramPoint.equals(c);
  }
  
  public final String toString()
  {
    return b;
  }
}
