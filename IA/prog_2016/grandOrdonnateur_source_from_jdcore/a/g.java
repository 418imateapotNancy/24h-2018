package a;

import a.a.d;
import java.awt.Point;

public abstract class g
{
  private k d;
  protected b a;
  private c e;
  protected Point b;
  protected int c = 10;
  private int f = 0;
  private int g = 0;
  private int h = 0;
  private int i = 0;
  private int j = 0;
  
  public g() {}
  
  public g(k paramK, b paramB, c paramC, Point paramPoint)
  {
    d = paramK;
    a = paramB;
    e = paramC;
    b = paramPoint;
  }
  
  public String toString()
  {
    return "Manutentionnaire : nom=" + e() + ", couleurVin=" + e + ", nbTours=" + f + ", nbActions=" + g + ", nbPointsPenalites=" + h + ", nbPointsPenalitesErreur=" + i + ", nbPointsDepots=" + j + ", score=" + t() + ", nbBouteillesDansSac=" + c;
  }
  
  public abstract String e();
  
  public abstract String f();
  
  public abstract boolean n();
  
  public final int p()
  {
    return f;
  }
  
  public final int q()
  {
    return g;
  }
  
  public final int r()
  {
    return h + i;
  }
  
  public final int s()
  {
    return j;
  }
  
  public final int t()
  {
    return a(f, g, h, i, j);
  }
  
  public static int a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    return paramInt1 * i.a.a() + paramInt2 * i.b.a() + paramInt3 + paramInt4 + paramInt5;
  }
  
  public final c u()
  {
    return e;
  }
  
  public final void a(c paramC)
  {
    e = paramC;
  }
  
  public final void a(Point paramPoint)
  {
    b = paramPoint;
  }
  
  public final Point v()
  {
    return b;
  }
  
  public final void a(b paramB)
  {
    a = paramB;
  }
  
  public final void a(k paramK)
  {
    d = paramK;
  }
  
  public final int w()
  {
    return c;
  }
  
  public final void x()
  {
    f += 1;
  }
  
  public final void y()
  {
    i += i.f.a();
  }
  
  public abstract a o();
  
  public void d(String paramString) {}
  
  public final boolean a(a paramA)
  {
    switch (h.a[paramA.ordinal()])
    {
    case 1: 
    case 2: 
    case 3: 
    case 4: 
      if (!a.a(b, paramA.d()))
      {
        h += i.c.a();
        paramA = 0;
      }
      else
      {
        paramA = paramA.d().b();
        paramA = new Point(b.x + x, b.y + y);
        if ((d != null) && (d.a() != null)) {
          d.a().b().a(e, b.x, b.y, x, y, c);
        }
        b = paramA;
        paramA = 1;
      }
      g += 1;
      break;
    case 5: 
      paramA = 1;
      if (!a.b(b))
      {
        h += i.e.a();
        paramA = 0;
      }
      if (!z())
      {
        h += i.d.a();
        paramA = 0;
      }
      if (paramA != 0) {
        try
        {
          a.c(b);
          c -= 1;
          if ((d != null) && (d.a() != null)) {
            d.a().b().a(e, b.x, b.y, a.a(b), this);
          }
          j += i.a(b, a.c());
        }
        catch (Exception localException)
        {
          paramA = 0;
        }
      }
      g += 1;
      break;
    case 6: 
      paramA = 1;
      if (!a.d(b))
      {
        h += i.c.a();
        paramA = 0;
      }
      if (!A())
      {
        h += i.d.a();
        paramA = 0;
      }
      if (paramA != 0) {
        c += 1;
      }
      g += 1;
      break;
    case 7: 
      paramA = 1;
      break;
    default: 
      throw new NullPointerException("L'action n'est pas définie.");
    }
    return paramA;
  }
  
  public final boolean z()
  {
    return c > 0;
  }
  
  public final boolean A()
  {
    return c < 10;
  }
}
