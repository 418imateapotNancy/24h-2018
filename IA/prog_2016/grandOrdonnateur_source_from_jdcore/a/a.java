package a;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum a
{
  private final String h;
  
  public static a[] a()
  {
    return (a[])i.clone();
  }
  
  private a(String paramString)
  {
    h = paramString;
  }
  
  public final char b()
  {
    return h.charAt(0);
  }
  
  public static char c()
  {
    return '?';
  }
  
  public static a a(char paramChar)
  {
    a[] arrayOfA;
    int j = (arrayOfA = a()).length;
    for (int k = 0; k < j; k++)
    {
      a localA;
      if ((localA = arrayOfA[k]).b() == paramChar) {
        return localA;
      }
    }
    return null;
  }
  
  public final n d()
  {
    n[] arrayOfN;
    int j = (arrayOfN = n.a()).length;
    for (int k = 0; k < j; k++)
    {
      n localN;
      if ((localN = arrayOfN[k]).c().equals(this)) {
        return localN;
      }
    }
    return null;
  }
  
  public static List e()
  {
    return new ArrayList(Arrays.asList(new a[] { a, b, c, d }));
  }
  
  public static List f()
  {
    return new ArrayList(Arrays.asList(new a[] { e, f }));
  }
}
