package a.a;

import a.k;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Point;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.border.TitledBorder;

public final class c
  extends JFrame
{
  public static final Color a = Color.WHITE;
  public static final Color b = new Color(128, 0, 128);
  private k c;
  private d d;
  private b e;
  
  public c() {}
  
  public c(k paramK, long paramLong)
  {
    super("Épreuve de développement de l'édition 2016 (Bordeaux) des 24h des IUT Informatique : l'une des caves " + bax + "x" + bay + " d'Emmanuel Bachusse.");
    c = paramK;
    getContentPane().setBackground(a);
    setBounds(0, 0, 1400, 900);
    paramK = this;
    paramK = this;
    d = new d(this, Math.min(865 / c.b().a().y, 845 / c.b().a().x), paramLong);
    d.setBackground(a);
    d.setPreferredSize(new Dimension(865, 845));
    d.setSize(getPreferredSize());
    add(d);
    e = new b(this);
    add(e, "East");
    paramK = new Dimension(500, 900);
    e.setPreferredSize(paramK);
    e.setSize(getPreferredSize());
    e.setBorder(new TitledBorder("Résultats des manutentionnaires"));
    e.setLayout(new BoxLayout(e, 1));
    setVisible(true);
  }
  
  public final k a()
  {
    return c;
  }
  
  public final d b()
  {
    return d;
  }
  
  public final b c()
  {
    return e;
  }
  
  public static void a(long paramLong)
  {
    if (paramLong > 0L) {
      try
      {
        Thread.sleep(paramLong);
        return;
      }
      catch (InterruptedException localInterruptedException)
      {
        (paramLong = localInterruptedException).printStackTrace();
      }
    }
  }
}
