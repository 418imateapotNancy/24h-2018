package a.a;

import a.g;
import a.k;
import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;

public final class b
  extends JPanel
{
  private c a;
  private JLabel b = new JLabel();
  private List c;
  
  public b() {}
  
  public b(c paramC)
  {
    a = paramC;
    setLayout(null);
    a();
    b.setHorizontalAlignment(0);
    b.setMaximumSize(new Dimension(32767, 32767));
    b.setFont(new Font("MONOSPACED", 0, 10));
    b.setForeground(c.b);
    add(b);
    c = new ArrayList();
    paramC = a.a().c().iterator();
    while (paramC.hasNext())
    {
      Object localObject = (g)paramC.next();
      localObject = new a((g)localObject);
      c.add(localObject);
      add(((a)localObject).b());
      add(((a)localObject).c());
      add(((a)localObject).d());
      add(((a)localObject).e());
      add(((a)localObject).f());
      add(((a)localObject).g());
      add(((a)localObject).h());
      add(((a)localObject).i());
    }
  }
  
  public final void a()
  {
    String str1 = "Historique : " + a.a().e();
    String str2 = "Pénalités  : " + a.a().d();
    String str3 = "";
    int i = str1.length() / 75;
    int j;
    int k;
    for (int m = Math.max(0, i - 5); m < i; m++)
    {
      j = m * 75;
      k = (m + 1) * 75;
      str3 = str3 + str1.substring(j, k) + "<BR>" + str2.substring(j, k) + ((m == i - 1) && (str1.length() % 75 == 0) ? "" : "<BR>");
    }
    if (str1.length() % 75 > 0)
    {
      j = i * 75;
      k = str1.length();
      str3 = str3 + str1.substring(j, k) + "<BR>" + str2.substring(j, k);
    }
    b.setText("<HTML>" + str3.replaceAll(" ", "&nbsp;") + "</HTML>");
  }
  
  public final List b()
  {
    return c;
  }
}
