package a.a;

import a.g;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JLabel;

public final class a
  extends JLabel
{
  private g a;
  private JLabel b = new JLabel();
  private JLabel c = new JLabel();
  private JLabel d = new JLabel();
  private JLabel e = new JLabel();
  private JLabel f = new JLabel();
  private JLabel g = new JLabel();
  private JLabel h = new JLabel();
  private JLabel i = new JLabel();
  
  public a() {}
  
  public a(g paramG)
  {
    a = paramG;
    paramG = new Dimension(32767, 32767);
    j();
    b.setOpaque(true);
    b.setText("");
    b.setBackground(a.u().b());
    b.setForeground(c.b);
    b.setHorizontalAlignment(0);
    b.setMaximumSize(paramG);
    c.setText("<HTML><CENTER>" + a.f().replaceAll(System.lineSeparator(), "<BR>") + "</HTML>");
    c.setFont(new Font(c.getFont().getFontName(), 1, 24));
    c.setForeground(c.b);
    c.setHorizontalAlignment(0);
    c.setMaximumSize(paramG);
    d.setForeground(c.b);
    d.setHorizontalAlignment(0);
    d.setMaximumSize(paramG);
    e.setForeground(c.b);
    e.setHorizontalAlignment(0);
    e.setMaximumSize(paramG);
    f.setForeground(c.b);
    f.setHorizontalAlignment(0);
    f.setMaximumSize(paramG);
    g.setForeground(c.b);
    g.setHorizontalAlignment(0);
    g.setMaximumSize(paramG);
    h.setFont(new Font(h.getFont().getFontName(), 1, 24));
    h.setForeground(c.b);
    h.setHorizontalAlignment(0);
    h.setMaximumSize(paramG);
    i.setForeground(c.b);
    i.setHorizontalAlignment(0);
    i.setMaximumSize(paramG);
  }
  
  public final g a()
  {
    return a;
  }
  
  public final JLabel b()
  {
    return b;
  }
  
  public final JLabel c()
  {
    return c;
  }
  
  public final JLabel d()
  {
    return d;
  }
  
  public final JLabel e()
  {
    return e;
  }
  
  public final JLabel f()
  {
    return f;
  }
  
  public final JLabel g()
  {
    return g;
  }
  
  public final JLabel h()
  {
    return h;
  }
  
  public final JLabel i()
  {
    return i;
  }
  
  public final void j()
  {
    d.setText("Nombre de tours : " + a.p());
    e.setText("Nombre d'actions : " + a.q());
    f.setText("Nombre total de points de pénalités : " + a.r());
    f.setVisible(a.r() != 0);
    g.setText("Nombre de points de dépôts de bouteilles : " + a.s());
    g.setVisible(a.s() != 0);
    h.setText("Score : " + a.t());
    i.setText("Nombre de bouteilles dans le sac : " + a.w());
  }
}
