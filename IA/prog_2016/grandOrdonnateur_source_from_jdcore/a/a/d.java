package a.a;

import a.b;
import a.g;
import a.k;
import java.awt.BasicStroke;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Line2D.Float;
import java.util.Iterator;
import java.util.List;

public final class d
  extends Canvas
{
  private static final Color a = Color.BLACK;
  private static final Color b = Color.GRAY;
  private static final Color c = Color.GREEN;
  private static final Color d = new Color(128, 0, 128);
  private static final Color e = Color.BLUE;
  private int f;
  private long g = 0L;
  private c h;
  
  public d() {}
  
  public d(c paramC, int paramInt, long paramLong)
  {
    h = paramC;
    f = paramInt;
    g = paramLong;
  }
  
  public final void paint(Graphics paramGraphics)
  {
    super.paint(paramGraphics);
    paramGraphics = (Graphics2D)paramGraphics;
    int i = h.a().b().a().x;
    int j = h.a().b().a().y;
    int n = j;
    int m = i;
    Graphics localGraphics = paramGraphics;
    d localD = this;
    localGraphics.setColor(b);
    localGraphics.setStroke(new BasicStroke(3.0F));
    int i1 = f / 3;
    for (int i2 = 1; i2 < n; i2++) {
      for (i3 = 0; i3 < m; i3++)
      {
        localGraphics.drawLine(10 + i2 * f, 10 + i3 * f, 10 + i2 * f, 10 + i3 * f + i1);
        localGraphics.drawLine(10 + i2 * f, 10 + (i3 + 1) * f, 10 + i2 * f, 10 + (i3 + 1) * f - i1);
      }
    }
    for (int i3 = 1; i3 < m; i3++) {
      for (i2 = 0; i2 < n; i2++)
      {
        localGraphics.drawLine(10 + i2 * f, 10 + i3 * f, 10 + i2 * f + i1, 10 + i3 * f);
        localGraphics.drawLine(10 + (i2 + 1) * f, 10 + i3 * f, 10 + (i2 + 1) * f - i1, 10 + i3 * f);
      }
    }
    n = j;
    m = i;
    localGraphics = paramGraphics;
    localD = this;
    localGraphics.setColor(a);
    localGraphics.setStroke(new BasicStroke(5.0F));
    localGraphics.drawRect(10, 10, n * f, m * f);
    a(paramGraphics, i, j);
    localGraphics = paramGraphics;
    localD = this;
    n = m = f / 5;
    i1 = m / 2;
    i2 = 10 + h.a().b().c().y * f + 4 * m - 4 + i1;
    i3 = 10 + h.a().b().c().x * f + 4 + i1;
    localGraphics.setColor(c);
    localGraphics.setStroke(new BasicStroke(3.0F));
    localGraphics.drawArc(i2 - i1, i3 - i1, n, n, 270, 270);
    localGraphics.drawLine(i2, i3, i2 - i1, i3);
    localGraphics.drawLine(i2, i3, i2 + (int)(Math.cos(3.9269908169872414D) * i1), i3 + (int)(Math.sin(3.9269908169872414D) * i1));
    localGraphics.drawLine(i2, i3, i2, i3 - i1);
    localGraphics.drawLine(i2, i3, i2 + (int)(Math.cos(5.497787143782138D) * i1), i3 + (int)(Math.sin(5.497787143782138D) * i1));
    localGraphics.drawLine(i2, i3, i2 + i1, i3);
    localGraphics.drawLine(i2, i3, i2 + (int)(Math.cos(0.7853981633974483D) * i1), i3 + (int)(Math.sin(0.7853981633974483D) * i1));
    localGraphics.drawLine(i2, i3, i2, i3 + i1);
    localGraphics = paramGraphics;
    localD = this;
    m = f / 5;
    n = 10 + h.a().b().c().y * f + 4 * m;
    i1 = 10 + h.a().b().c().x * f + 1 * m;
    i2 = n;
    i3 = i1 + 3 * m;
    i = i2 - 2 * m;
    j = i3;
    int k = i - m;
    int i4 = j;
    int i5 = k;
    int i6 = i4 - m;
    int i7 = i5;
    int i8 = i6 - 2 * m;
    localGraphics.setColor(a.c.a.b());
    localGraphics.fillPolygon(new int[] { n, i2, i }, new int[] { i1, i3, j }, 3);
    localGraphics.setColor(a.c.b.b());
    localGraphics.fillPolygon(new int[] { n, i, k, i5 }, new int[] { i1, j, i4, i6 }, 4);
    localGraphics.setColor(a.c.c.b());
    localGraphics.fillPolygon(new int[] { n, i5, i7 }, new int[] { i1, i6, i8 }, 3);
    localGraphics.setColor(d);
    localGraphics.setStroke(new BasicStroke(3.0F));
    localGraphics.drawRect(i7, i8, 3 * m, 3 * m);
    localGraphics.drawLine(n, i1, i, j);
    localGraphics.drawLine(n, i1, i5, i6);
    a(paramGraphics);
  }
  
  private void a(Graphics2D paramGraphics2D, int paramInt1, int paramInt2)
  {
    for (int i = 0; i < paramInt1; i++) {
      for (int j = 0; j < paramInt2; j++)
      {
        int n = h.a().b().d()[i][j];
        int m = j;
        int k = i;
        Graphics2D localGraphics2D1 = paramGraphics2D;
        d localD1 = this;
        for (int i1 = 0; i1 < n; i1++)
        {
          int i4 = i1;
          int i3 = m;
          int i2 = k;
          Graphics2D localGraphics2D2 = localGraphics2D1;
          d localD2;
          (localD2 = localD1).a(localGraphics2D2, i2, i3, i4, null);
        }
      }
    }
  }
  
  private void a(Graphics2D paramGraphics2D, int paramInt1, int paramInt2, int paramInt3, Color paramColor)
  {
    int i = f / 5;
    int j = paramInt3 % 3;
    paramInt3 = (paramInt3 - j) / 3;
    paramInt2 = 10 + paramInt2 * f + (j + 1) * i;
    paramInt1 = 10 + paramInt1 * f + (3 - paramInt3) * i;
    paramInt3 = (int)(0.9F * i);
    paramGraphics2D.setStroke(new BasicStroke(2.0F));
    if (paramColor == null)
    {
      paramGraphics2D.setColor(e);
      paramGraphics2D.drawOval(paramInt2, paramInt1, paramInt3, paramInt3);
      return;
    }
    paramGraphics2D.setColor(paramColor);
    paramGraphics2D.fillOval(paramInt2, paramInt1, paramInt3, paramInt3);
  }
  
  private void a(Graphics2D paramGraphics2D)
  {
    Iterator localIterator = h.a().c().iterator();
    while (localIterator.hasNext())
    {
      g localG = (g)localIterator.next();
      a(paramGraphics2D, localG.u(), vx, vy, localG.u().b(), localG.w());
    }
  }
  
  private void a(Graphics2D paramGraphics2D, a.c paramC, int paramInt1, int paramInt2, Color paramColor, int paramInt3)
  {
    Color localColor = f / 5;
    switch (e.a[paramC.ordinal()])
    {
    case 1: 
      paramC = 10 + paramInt2 * f + 4 * localColor - 4;
      paramInt1 = 10 + paramInt1 * f + 4 * localColor - 4;
      break;
    case 2: 
      paramC = 10 + paramInt2 * f + 4;
      paramInt1 = 10 + paramInt1 * f + 4 * localColor - 4;
      break;
    case 3: 
      paramC = 10 + paramInt2 * f + 4;
      paramInt1 = 10 + paramInt1 * f + 4;
      break;
    default: 
      throw new NullPointerException("La couleur de vin n'est pas définie.");
    }
    paramGraphics2D.setColor(paramColor);
    paramGraphics2D.setStroke(new BasicStroke(1.0F));
    paramGraphics2D.fillRect(paramC, paramInt1, localColor, localColor);
    if (paramColor != c.a)
    {
      paramColor = localColor;
      paramInt2 = paramInt3;
      paramInt1 = paramInt1;
      paramC = paramC;
      paramGraphics2D = paramGraphics2D;
      paramInt3 = paramColor - 2;
      paramInt1 = paramInt1 + paramColor - 1;
      paramGraphics2D.setColor(Color.RED);
      paramGraphics2D.setStroke(new BasicStroke(2.0F));
      Line2D.Floatx1 = (paramC + 1);
      x2 = (x1 + paramInt3);
      y1 = paramInt1;
      y2 = paramInt1;
      paramGraphics2D.draw(paramColor);
      paramGraphics2D.setColor(Color.GREEN);
      paramGraphics2D.setStroke(new BasicStroke(2.0F));
      Line2D.Floatx1 = x1;
      x2 = (x1 + paramInt2 / 10.0F * paramInt3);
      y1 = paramInt1;
      y2 = paramInt1;
      paramGraphics2D.draw(paramC);
    }
  }
  
  public final void a(a.c paramC, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    c.a(g);
    Graphics2D localGraphics2D = (Graphics2D)getGraphics();
    int j = paramInt2;
    int i = paramInt1;
    a.c localC = paramC;
    paramInt2 = localGraphics2D;
    paramInt1 = this;
    a(paramInt2, localC, i, j, c.a, 0);
    a(localGraphics2D, paramC, paramInt3, paramInt4, paramC.b(), paramInt5);
  }
  
  public final void a(a.c paramC, int paramInt1, int paramInt2, int paramInt3, g paramG)
  {
    c.a(g);
    a((Graphics2D)getGraphics(), paramInt1, paramInt2, paramInt3, paramC.b());
    paramInt2 = paramG;
    paramInt1 = (Graphics2D)getGraphics();
    paramC = this;
    a(paramInt1, paramInt2.u(), vx, vy, paramInt2.u().b(), paramInt2.w());
  }
}
