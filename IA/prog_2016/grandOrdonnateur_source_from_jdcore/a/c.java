package a;

import java.awt.Color;

public enum c
{
  private final Color d;
  private final String e;
  
  public static c[] a()
  {
    return (c[])f.clone();
  }
  
  private c(Color paramColor, String paramString)
  {
    d = paramColor;
    e = paramString;
  }
  
  public final Color b()
  {
    return d;
  }
  
  public final String c()
  {
    return e;
  }
  
  public static c d()
  {
    if (a().length == 0) {
      return null;
    }
    return a()[0];
  }
  
  public final c e()
  {
    if ((a().length == 0) || (this == null)) {
      return null;
    }
    return a()[((ordinal() + 1) % a().length)];
  }
}
