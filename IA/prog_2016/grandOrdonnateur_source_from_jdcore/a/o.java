package a;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class o
{
  public static void a(Logger paramLogger, Exception paramException)
  {
    paramLogger = paramException;
    paramException = new StringWriter();
    PrintWriter localPrintWriter = new PrintWriter(paramException);
    paramLogger.printStackTrace(localPrintWriter);
    paramLogger.severe(paramException.toString());
  }
  
  private static int a(String paramString1, String paramString2)
  {
    Integer localInteger = null;
    try
    {
      localInteger = new Integer(paramString1);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      System.out.println(paramString2 + " doit etre un entier. Abandon.");
      System.exit(1);
    }
    return localInteger.intValue();
  }
  
  private static Long b(String paramString1, String paramString2)
  {
    Long localLong = null;
    try
    {
      localLong = new Long(paramString1);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      System.out.println(paramString2 + " doit etre un entier. Abandon.");
      System.exit(1);
    }
    return localLong;
  }
  
  public static String a(String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2)
  {
    if ((paramArrayOfString1.length > paramArrayOfString2.length) || (paramString == null)) {
      throw new IllegalArgumentException();
    }
    for (int i = 0; (i < paramArrayOfString1.length) && (!paramString.equals(paramArrayOfString1[i])); i++) {}
    if (i >= paramArrayOfString1.length) {
      throw new IllegalArgumentException("nom d'argument " + paramString + " non trouvé.");
    }
    return paramArrayOfString2[i];
  }
  
  public static int b(String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2)
  {
    return a(paramArrayOfString1 = a(paramString, paramArrayOfString1, paramArrayOfString2), paramString);
  }
  
  public static Long c(String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2)
  {
    return b(paramArrayOfString1 = a(paramString, paramArrayOfString1, paramArrayOfString2), paramString);
  }
  
  public static void a(String paramString1, String paramString2, String paramString3)
  {
    (localObject = System.getProperties()).setProperty("mail.smtp.host", j.c());
    Object localObject = Session.getDefaultInstance((Properties)localObject);
    localObject = new MimeMessage((Session)localObject);
    try
    {
      ((MimeMessage)localObject).setFrom(new InternetAddress("gentil.organisateur@u-bordeaux.fr"));
      ((MimeMessage)localObject).addRecipient(Message.RecipientType.TO, new InternetAddress("\"" + paramString1 + "\""));
      paramString1 = j.d().iterator();
      while (paramString1.hasNext())
      {
        String str = (String)paramString1.next();
        ((MimeMessage)localObject).addRecipient(Message.RecipientType.BCC, new InternetAddress(str));
      }
      ((MimeMessage)localObject).setSubject(paramString2);
      ((MimeMessage)localObject).setText(paramString3 + System.lineSeparator() + "Bonne chance pour la suite." + System.lineSeparator() + "Le gentil organisateur de l'épreuve de développement de l'édition 2016 (Bordeaux) des 24h des IUT Informatique." + System.lineSeparator());
      Transport.send((Message)localObject);
      return;
    }
    catch (MessagingException localMessagingException)
    {
      (paramString1 = localMessagingException).printStackTrace();
    }
  }
}
