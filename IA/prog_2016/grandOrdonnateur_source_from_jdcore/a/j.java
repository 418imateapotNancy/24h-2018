package a;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

public final class j
{
  private static boolean a = false;
  private static Properties b;
  private static String c = null;
  private static Map d = new HashMap();
  
  public static Properties a()
  {
    if (!a)
    {
      b = new Properties();
      Object localObject;
      if ((localObject = (localObject = System.getProperties()).getProperty("fichier_proprietes")) == null) {
        localObject = "24h.properties";
      }
      try
      {
        FileInputStream localFileInputStream = new FileInputStream((String)localObject);
        b.load(localFileInputStream);
        localFileInputStream.close();
      }
      catch (FileNotFoundException localFileNotFoundException)
      {
        System.err.println("Fichier non trouve : " + (String)localObject);
      }
      catch (IOException localIOException)
      {
        System.err.println("IOException en lisant " + (String)localObject);
      }
      a = true;
    }
    return b;
  }
  
  private static String b(String paramString)
  {
    a();
    return b.getProperty(paramString);
  }
  
  public static String b()
  {
    return b("repertoire_journal_equipe_linux");
  }
  
  public static String c()
  {
    return b("serveur_smtp");
  }
  
  public static Collection d()
  {
    a();
    HashSet localHashSet = new HashSet();
    int i = 1;
    String str = "destinataire_mail_" + 1;
    for (str = b.getProperty(str); str != null; str = b.getProperty(str))
    {
      localHashSet.add(str);
      i++;
      str = "destinataire_mail_" + i;
    }
    return localHashSet;
  }
  
  public static void a(String paramString)
  {
    c = paramString;
  }
  
  public static void a(Logger paramLogger)
  {
    a(paramLogger, null);
  }
  
  public static void a(Logger paramLogger, String paramString)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    if (c != null)
    {
      localStringBuffer.append(c);
      localStringBuffer.append(System.getProperty("file.separator"));
    }
    if (paramString != null)
    {
      localStringBuffer.append(paramString);
      localStringBuffer.append(System.getProperty("file.separator"));
    }
    paramString = new File(localStringBuffer.toString());
    if ((localStringBuffer.length() > 0) && (!paramString.exists()))
    {
      System.out.println("tentative de création de " + paramString);
      try
      {
        paramString.mkdir();
      }
      catch (Exception localException)
      {
        paramLogger.severe("Impossible de créer le répertoire " + paramString + " pour y stocker le journal");
        localException.printStackTrace();
      }
    }
    localStringBuffer.append("LOG_").append(paramLogger.getName()).append(".xml");
    try
    {
      FileHandler localFileHandler = new FileHandler(localStringBuffer.toString());
      d.put(paramLogger, localFileHandler);
      paramLogger.addHandler(localFileHandler);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      paramLogger.severe("Impossible d'associer le FileHandler ; SecurityException = " + localSecurityException.toString());
      localSecurityException.printStackTrace();
      return;
    }
    catch (IOException localIOException)
    {
      paramLogger.severe("Impossible d'associer le FileHandler ; IOException = " + localIOException.toString());
      localIOException.printStackTrace();
    }
  }
}
