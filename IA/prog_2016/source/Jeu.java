import java.util.ArrayList;


/**
 * cette classe omg
 * @author blondin
 *
 */
public class Jeu {
	//posCla = position dans la liste joueur
	//nbBout = nombre de bourteils dans le sacs
	//escalier coordonnees de l escalier
	int largeur, hauteur, posCla, nbJ, nbBout;
	Casier casier;
	ArrayList<Joueur> joueur;
	Coordonnees escalier;

	public Jeu(int largeur, int hauteur, int posCla, String c, int nbJ){
		this.largeur=largeur;
		this.hauteur=hauteur;
		this.posCla=posCla;
		this.casier = new Casier(this.largeur,this.hauteur);
		extraireCasiers(c);
		this.nbJ=nbJ;
		int esc=0;
		this.nbBout=10;
		//try{
			this.joueur=new ArrayList<Joueur>();
			System.out.println("coo esca :"+  escalier.x +" " +escalier.y );
			for(int i=0; i<nbJ;i++)
				this.joueur.add(new Joueur(escalier.x,escalier.y));
		/*}catch(ArrayIndexOutOfBoundsException e){
			System.out.println("escalier non trouve");
			System.err.println(e);
			System.err.println(e.getStackTrace());
		}*/
	}



	/**
	 * créer la variable casier dans jeu,
	 * casier est un tableau de deux dimensions contenant les contenance des casier
	 * @param chaineCasier
	 * @return
	 */
	public void extraireCasiers(String chaineCasier) {
		for(int i=0;i<this.casier.getWidth();i++){
			for(int j=0;j<this.casier.getHeight();j++){
				int index = j*this.largeur+i;
				
				if(chaineCasier.charAt(index) == '@'){
					this.casier.set(i,j,-1);
					escalier = new Coordonnees(i, j);
				}else{
					this.casier.set(i,j,chaineCasier.charAt(index) - '0');
				}

			}
		}
	}

	public int getPosCla() {
		return posCla;
	}



	public void setPosCla(int posCla) {
		this.posCla = posCla;
	}



	public int getNbBout() {
		return nbBout;
	}



	public void setNbBout(int nbBout) {
		this.nbBout = nbBout;
	}



	public int getNbJ() {
		return nbJ;
	}



	public Casier getCasier() {
		return casier;
	}

	public ArrayList<Joueur> getJoueur() {
		return joueur;
	}



	public Coordonnees getEscalier() {
		return escalier;
	}



	public void miseAJour(int numjoueur, String dep){
		Joueur j=this.joueur.get(numjoueur);
		boolean continu = false;
			Coordonnees coord = j.getCoord();
			for (char c : dep.toCharArray())
			{
				switch (c)
				{
				case 'N':
					if(coord.y-1>=0)
						j.setY(-1);
					break;
				case 'S':
					if(coord.y+1<this.hauteur)
						j.setY(1);
					break;
				case 'E':
					if(coord.x+1<this.largeur)
						j.setX(1);
					break;
				case 'O':
					if(coord.x-1>=0)
						j.setX(-1);
					break;
				case 'P':
					if(casier.get(coord.x,coord.y)>0){
						casier.set(coord.x,coord.y, casier.get(coord.x,coord.y)-1);
						j.ajoutNbBouteille(-1);
					}
					break;
				case 'R':
				if(coord.equals(escalier))
					j.ajoutNbBouteille(1);
				case 'I':
					continu=true;
					break;
				}
				if(continu)
					break;
			}
	}
}
