/**
 * class representant le joueur
 * @author blondin
 *
 */
public class Joueur {
	/**
	 * attribut, position, nombre de bouteille
	 */
	private Coordonnees coord;
	private int nbBouteille;
	
	/**
	 * constructeur de joueur
	 */
	public Joueur(int x,int y){
		this.coord=new Coordonnees(x,y);
		this.nbBouteille=10;
	}
	
	public int getX(){
		return this.coord.x;
	}
	
	public void setX(int x){
		this.coord.x+=x;
	}
	
	public void setY(int y){
		this.coord.y+=y;
	}
	
	public int getY(){
		return this.coord.y;
	}
	
	public Coordonnees getCoord(){
		return this.coord;
	}
	
	public void set(Coordonnees c){
		this.coord=c;
	}
	
	public int getNbBouteille(){
		return this.nbBouteille;
	}
	
	public void ajoutNbBouteille(int nb){
		if(this.nbBouteille+nb<=10 && this.nbBouteille+nb>=0){
			this.nbBouteille+=nb;
		}
	}
	
}
