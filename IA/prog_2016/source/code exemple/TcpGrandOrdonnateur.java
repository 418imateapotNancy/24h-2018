/**
 * Remarques du gentil organisateur destin�es aux comp�titeurs : 
 * - utilisez cette classe pour communiquer avec le Grand Ordonnateur
 * - ne changez rien au code de cette classe 
 * 
 * Interface pour le protocole du Grand Ordonnateur.
 */

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.regex.Pattern;

/**
 * Interface pour le protocole du Grand Ordonnateur.
 * 
 * @author Olivier plagiant �hont�ment Olivier (l'autre)
 */
public class TcpGrandOrdonnateur {

	/**
	 * S�parateur entre les cha�nes de caract�res �chang�es par TCP.
	 */
	public static final char SEPARATEUR = '~';

	/**
	 * Caract�re minimal (inclus) autoris� par le protocole du Grand
	 * Ordonnateur.
	 */
	public static final int CARACTERE_MINIMAL_INCLUS = 32;

	/**
	 * Caract�re maximal (exclu) autoris� par le protocole du Grand Ordonnateur.
	 */
	public static final int CARACTERE_MAXIMAL_EXCLU = (int) SEPARATEUR;

	/**
	 * Socket.
	 */
	private Socket socket;

	/**
	 * Cr�ation d'une interface pour le protocole du Grand Ordonnateur.
	 */
	public TcpGrandOrdonnateur() {
		socket = null;
	}

	/**
	 * Connexion au serveur.
	 * 
	 * @param hote
	 *            H�te.
	 * @param port
	 *            Port.
	 * @throws IOException
	 */
	public void connexion(String hote, int port) throws IOException {
		socket = new Socket(hote, port);
		socket.getInputStream();
		socket.getOutputStream();
	}

	/**
	 * Envoi d'un entier au serveur.
	 * 
	 * @param entier
	 *            Entier � envoyer.
	 * @throws IOException
	 */
	public void envoiEntier(int entier) throws IOException {
		testeSocket();
		final OutputStream fluxSortie = socket.getOutputStream();
		if (fluxSortie == null) {
			throw new IllegalArgumentException("Le flux en sortie est null.");
		} else {
			fluxSortie.write(entier);
			fluxSortie.flush();
		}
	}

	/**
	 * Envoi d'un caract�re au serveur.
	 * 
	 * @param caractere
	 *            Caract�re � envoyer.
	 * @throws IOException
	 */
	public void envoiCaractere(char caractere) throws IOException {
		envoiEntier(caractere);
	}

	/**
	 * Envoi d'une cha�ne de caract�res au serveur.
	 * 
	 * @param chaine
	 *            Cha�ne de caract�res � envoyer.
	 * @throws IOException
	 */
	public void envoiChaine(String chaine) throws IOException {
		if (chaine != null) {
			if (chaineValidePourTransmission(chaine)) {
				for (char caractere : chaine.toCharArray()) {
					envoiEntier(caractere);
				}
				envoiEntier(SEPARATEUR);
			} else {
				throw new IllegalArgumentException(
						"La cha�ne de caract�res � envoyer � " + chaine + " � n'est pas valide.");
			}
		}
	}

	/**
	 * Teste si une cha�ne de caract�res est valide (dans un certain intervalle
	 * de la table ASCII (cf. https://fr.wikipedia.org/wiki/
	 * American_Standard_Code_for_Information_Interchange)) pour �tre envoy�e,
	 * c'est-�-dire si elle ne contient pas de caract�re sp�cial.
	 * 
	 * @param chaine
	 *            Cha�ne de caract�res � envoyer.
	 * @return La cha�ne de caract�res est-elle valide pour �tre envoy�e ?
	 */
	private boolean chaineValidePourTransmission(String chaine) {
		return chaine == null ? false
				: Pattern.matches(
						"[" + ((char) CARACTERE_MINIMAL_INCLUS) + "-" + ((char) (CARACTERE_MAXIMAL_EXCLU - 1)) + "]*",
						chaine);
	}

	/**
	 * R�ception d'un entier depuis le serveur.
	 * 
	 * @return Entier re�u.
	 * @throws IOException
	 */
	public int receptionEntier() throws IOException {
		testeSocket();
		final InputStream fluxEntree = socket.getInputStream();
		if (fluxEntree == null) {
			throw new IllegalArgumentException("Le flux en entr�e est null.");
		} else {
			int entierRecu = -1;
			entierRecu = fluxEntree.read();
			return entierRecu;
		}
	}

	/**
	 * R�ception d'un caract�re depuis le serveur.
	 * 
	 * @return Caract�re re�u.
	 * @throws IOException
	 */
	public char receptionCaractere() throws IOException {
		return (char) receptionEntier();
	}

	/**
	 * R�ception d'une cha�ne de caract�res depuis le serveur.
	 * 
	 * @return Cha�ne de caract�res re�ue.
	 * @throws IOException
	 */
	public String receptionChaine() throws IOException {
		testeSocket();
		final StringBuffer chaine = new StringBuffer();
		boolean separateurRecu = false;
		boolean ok = true;
		while (ok && !separateurRecu) {
			final char recu = (char) receptionEntier();
			ok = (recu != (char) -1);
			separateurRecu = (recu == SEPARATEUR);
			if (ok && !separateurRecu) {
				chaine.append(recu);
			}
		}
		return ok ? chaine.toString() : null;
	}

	/**
	 * D�connexion du serveur.
	 * 
	 * @throws IOException
	 */
	public void deconnexion() throws IOException {
		testeSocket();
		InputStream fluxEntree = null;
		fluxEntree = socket.getInputStream();
		if (fluxEntree != null) {
			fluxEntree.close();
		}
		OutputStream fluxSortie = null;
		fluxSortie = socket.getOutputStream();
		if (fluxSortie != null) {
			fluxSortie.close();
		}
		socket.close();
	}

	/**
	 * Teste si le socket est initialis� ; sinon, arr�t du programme.
	 */
	private void testeSocket() {
		if (socket == null) {
			System.out.println("Le socket n'est pas initialis�.");
			System.out.flush();
			System.exit(1);
		}
	}

}
