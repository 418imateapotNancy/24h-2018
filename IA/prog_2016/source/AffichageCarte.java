import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.ArrayList;

public class AffichageCarte extends JPanel{

		private Carte c;
		private JFrame fenetre;
		private List<Color> colors;
		private int[][] distances;
		private double[][] scores;
		private int affichage;
		private double coeffScore;
		public static final int AFFICHER_SCORE = 0, AFFICHER_DISTANCE = 1;
		
		public AffichageCarte(Carte c)
		{
			this.c = c;
			colors = new ArrayList<Color>();
			colors.add(null);
			colors.add(Color.black);
			colors.add(Color.green);
			colors.add(Color.blue);
			colors.add(Color.red);
			colors.add(Color.yellow);
			colors.add(Color.magenta);
			colors.add(Color.gray);
			
			fenetre = new JFrame("Affichage de la carte");
			
			fenetre.add(this);
			fenetre.setSize(c.getWidth() * 40, c.getHeight() *40);
			fenetre.setVisible(true);
			
			distances = null;
			scores = null;	
			affichage = AFFICHER_DISTANCE;
			
			setFocusable(true);
			requestFocusInWindow();
			
			addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent e)
				{
					if (e.getKeyChar() == 's')
						setAffichage(AFFICHER_SCORE);
					else if  (e.getKeyChar() == 'd')
						setAffichage(AFFICHER_DISTANCE);
				}
			});
			
			coeffScore = 10;
		}
		
		public void setCoeffScore(double c)
		{
			coeffScore = c;
		}
		
		public void setDistances(int[][] dist) throws InvalidParameterException
		{
			if (dist.length != c.getWidth() || dist[0].length != c.getHeight())
				throw new InvalidParameterException("Carte and distances length mismatch");
			distances = dist;
		}
		
		public void setScores(double[][] scores) throws InvalidParameterException
		{
			if (scores.length != c.getWidth() || scores[0].length != c.getHeight())
				throw new InvalidParameterException("Carte and distances length mismatch");
			this.scores = scores;
		}
		
		public void afficherCarte()
		{
			repaint();
		}
		
		public void setAffichage(int a)
		{
			affichage = a;
			repaint();
		}
		
		public void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			
			int w = getWidth(), h = getHeight();
			int carteW = c.getWidth(), carteH = c.getHeight();
			int caseW = w / carteW, caseH = h / carteH;
			
			for (int i = 0; i < carteW; i++)
			{
				for (int j = 0; j < carteH; j++)
				{
					int type = c.getTypeCase(i, j);
					int x = i * caseW, y = j * caseH;
					g.setColor(Color.black);
					g.drawRect(x, y, caseW, caseH);
					
					if (affichage == AFFICHER_DISTANCE)
					{
						if (distances != null)
						{
							int v = distances[i][j];
							v = (v == -1 ? 0 : 255 / (v + 1));
							v = v > 255 ? 255 : v < 0 ? 0 : v;
							g.setColor(new Color(v,v,v));
							g.fillRect(x + 1, y + 1, caseW - 1, caseH - 1);
							g.setColor(new Color(200, 200, 0));
						}
					}
					else
					{
						if (scores != null)
						{
							int r = (int)(scores[i][j] * coeffScore);
							r = (r != 0 ?r + 40 : 0);
							r = r > 255 ? 255 : r < 0 ? 0 : r;
							g.setColor(new Color(r,0,0));
							g.fillRect(x + 1, y + 1, caseW - 1, caseH - 1);
							g.setColor(Color.white);
						}
					}
					
					
						g.drawString("" + type, x + 5, y + caseH - 2);
				}
			}
		}
}
