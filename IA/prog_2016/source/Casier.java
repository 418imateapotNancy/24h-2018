import java.util.Arrays;

/**
 * to con
 * classe qui sert de base pour le tableau a 2 dimensions pour les casiers
 * @author blondin
 *
 */
public class Casier implements Carte{
private int[][] carte;
private boolean change;

	public Casier(int w, int h)
	{
		w = w < 1 ? 1 : w;
		h = h < 1 ? 1 : h;
		carte = new int[w][h];
		for (int[] row : carte)
			Arrays.fill(row, 0);
        change = false;
	}

	public void set(int x, int y, int type)
	{
		carte[x][y] = type;
		change = true;
	}

	public int get(int x, int y)
	{
		return carte[x][y];
	}

	@Override
	public int getWidth() {
		return carte.length;
	}

	@Override
	public int getHeight() {
		return carte.length == 0 ? 0 : carte[0].length;
	}

	@Override
	public boolean estTraversable(int x, int y) {
		return true;
	}

	@Override
	public int getTypeCase(int x, int y) {
		return carte[x][y];
	}

	@Override
	public boolean aChange()
	{
	    boolean res = change;
	    change = false;
	    return res;
	}


}
