
public class InvalidParameterException extends Exception {
	public InvalidParameterException()
	{
		super();
	}
	
	public InvalidParameterException(String s)
	{
		super(s);
	}
}
