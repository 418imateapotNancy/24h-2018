import java.util.*;

public class Decision {

	private int[][] distance, tabPoints, distanceEscalier;
	private Casier casier;
	private Jeu jc;
	private int nbAction;
	private int nbBouteilles;
	private Joueur joueur;
	private Pathfinding pathfinder = null;
	private int xMax, yMax;
	private int placeJoueur;
	private String resFinal,res;

	public Decision(Jeu j,int p){
		//recuperer le nombre de bouteille
		this.jc=j;
		this.placeJoueur = p;
		this.joueur = this.jc.getJoueur().get(this.placeJoueur);
		pathfinder = new Pathfinding(jc.getCasier());
		distanceEscalier = pathfinder.calculerDistances(j.getEscalier().x, j.getEscalier().y);
	}

	/**
	 * Prend la decision (appel dans le main)
	 * @return
	 */
	public String prendreDecision(){
		
		this.nbAction = 0;
		this.resFinal="";
		this.distance = pathfinder.calculerDistances(this.joueur.getX(), this.joueur.getY());
		this.casier = jc.getCasier();
		this.nbBouteilles = joueur.getNbBouteille();

		//trouver le plus
		tabPoints = this.avoirPoints(this.distance, this.casier);
		//Trouve le nombre de points le plus important dans le tableau
		nbPointsMax(tabPoints);

		res = "";
		//prise de decision
		if(this.nbBouteilles >=5){
			this.actions();
		}
		else if(this.nbBouteilles <5 && this.nbBouteilles >2){
			int coef = 5 -this.nbBouteilles;
			coef*=15;
			tabPoints = avoirPointsCoeff(tabPoints, coef);
			nbPointsMax(tabPoints);
			//refaire au plus
			this.actions();
		}
		else{
			this.retournerBase();
		}
		jc.miseAJour(placeJoueur, res);

		System.out.println("chaine retourne :" + resFinal);
		return resFinal;
	}

	private void nbPointsMax(int[][] tab){
		int max = 0;

		for(int i = 0; i < tab.length; i++){
			for(int j = 0; j < tab[i].length; j++){
				if(max < tab[i][j]){
					max = tab[i][j];
					xMax = i; yMax = j;
				}
			}
		}

	}

	/**
	* Calcule les points associes a une case en fonction de sa distance au joueur et de son type (nb Bouteilles ou -1 pour escalier)
	*/
	private double calculerPoint(int distJou, int distEsc, int type)
	{
		int nbActionsApres = 7 - nbAction - distJou;
		int nbBouteilles = type < 0 ? 0 : type;
		
		int nbBouteillePosable = Math.min(type, nbActionsApres);
		return (4 * distEsc + 4) * nbBouteillePosable - distJou;
	}
	/**
	 * Avoir le tableau des points
	 * @param distance
	 * @param casiers
	 * @return
	 */
	public int[][] avoirPoints(int[][] distance, Casier casiers){
		// On suppose que distance et casiers ont la meme taille et corresponde a la taille de la carte
		// On suppose que le tableau "casiers" contient seulement les casiers non rempli
		int[][] res = new int[distance.length][distance[0].length];

		for(int i = 0; i < res.length; i++){
			for(int j = 0; j < res[i].length; j++){
				System.out.print(casiers.get(i, j));
				System.out.print("|"+calculerPoint(distance[i][j], distanceEscalier[i][j], casiers.get(i, j))+" ");
				res[i][j] = (int)calculerPoint(distance[i][j], distanceEscalier[i][j], casiers.get(i, j));
			}
			System.out.println("");
		}

		return(res);
	}

	public int[][] avoirPointsCoeff(int[][] tab, int coeff){
		// On suppose que distance et casiers ont la meme taille et corresponde a la taille de la carte
		// On suppose que le tableau "casiers" contient seulement les casiers non rempli
		int jx = joueur.getX();
		int jy = joueur.getY();
		int[][] res = new int[tab.length][tab[0].length];

		for(int x = 0; x < res.length; x++){
			for(int y = 0; y < res[x].length; y++){
				if(((jx == x+1) && (jy == y)) || ((jx == x-1) && (jy == y)) ||
				   ((jx == x) && (jy == y+1)) || ((jx == x) && (jy == y-1))){
						 res[x][y] = tab[x][y] * coeff;
					 } else{
						 res[x][y] = tab[x][y];
					 }
			}
		}

		return(res);
	}

	/**
	 * Decide des actions en fonctions de où est le plus gros score
	 * @param xGros
	 * @param yGros
	 * @return
	 */
	public void actions(){
		//on va sur la case
		this.allerA(xMax, yMax);

		while(this.nbAction <7 && (casier.get(xMax,yMax) >0) && joueur.getNbBouteille()>0){
			res+="P";
			resFinal += "P";
			this.nbAction++;
		}
		if(joueur.getNbBouteille()==0){
			this.retournerBase();
		}
		else if (this.nbAction < 7){
			jc.miseAJour(placeJoueur, res);
			res ="";
			this.joueur = this.jc.getJoueur().get(this.placeJoueur);
			this.distance = pathfinder.calculerDistances(this.joueur.getX(), this.joueur.getY());
			this.casier = jc.getCasier();
			tabPoints = avoirPoints(distance, casier);
			nbPointsMax(tabPoints);
			actions();
		}
	}

	public void retournerBase(){
		allerA(jc.getEscalier().x,jc.getEscalier().y);
		while(this.nbAction < 7 && this.nbBouteilles != 10){
			res+="R";
			resFinal += "R";
			this.joueur.ajoutNbBouteille(1);
			this.nbAction++;
		}
		if(this.nbAction <7){
			res+="I";
			resFinal +="I";
		}
	}

	public void allerA(int xVoulue,int yVoulue){

		//On recupere les chemins possibles
		 List<List<Integer>> listeChemins = pathfinder.directionsPourAller(joueur.getX(), joueur.getY(), xVoulue, yVoulue);
		 //On recupere le premier chemin (si on compare pas les differents chemins possibles)
		 List<Integer> chemin = listeChemins.get(0);
		 //Pour faire la conversion entre entier et caractere pour les directions
		 Character[] tabDirs = {'N', 'E', 'S', 'O'};
		 Character action;
		
		 for (int dir : chemin)
		 {
			 //Si la case est impossible a atteindre, on ne fait rien et on quitte
			 if (dir == Pathfinding.DIRECTION_IMPOSSIBLE)
				 action = 'I';
			 else if (dir == Pathfinding.DIRECTION_CENTRE) //Si on est deja sur la case, c'est bon
				 break;
			 else//On recupere le caractere associe a la direction
				 action = tabDirs[dir];
			res += action; 
			resFinal += action;
			nbAction++;
		 }
	}

	public int[][] getTabPoints(){
		return (this.tabPoints);
	}

	public int[][] getDistances(){
		return (this.distance);
	}
}
