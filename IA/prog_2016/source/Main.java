/**
 * Illustration du protocole du Grand Ordonnateur.
 */

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;

/**
 * Illustration du protocole du Grand Ordonnateur, fournie par le gentil

 * organisateur de l'épreuve de développement des 24h IUT Informatique de
 * Bordeaux 2016 (OG&OG).
 *
 * @author Olivier
 */
public class Main {

	/**
	 * Hôte du Grand Ordonnateur.
	 */
	private String hote = null;

	/**
	 * Port du Grand Ordonnateur.
	 */
	private int port = -1;
	private int tour, place, nbJoueurs;

	private Jeu jeu;
	private Decision decision;
	private AffichageCarte affichage;

	/**
	 * Interface pour le protocole du Grand Ordonnateur.
	 */
	private TcpGrandOrdonnateur tcpGdOrdo = null;

	/**

	 * Cr�ation d'une illustration du protocole du Grand Ordonnateur.
	 *
	 * @param hote
	 *            H�te.
	 * @param port
	 *            Port.
	 */
	public Main(String hote, int port) {
		this.hote = hote;
		this.port = port;
		this.tcpGdOrdo = new TcpGrandOrdonnateur();
	}

	private void init(String nomEquipe, String nomIUT) throws IOException
	{
	    	// Finalisation du « create » du protocole du Grand Ordonnateur.
		// - connexion
		tcpGdOrdo.connexion(hote, port);
		// Réponse au « create » du protocole du Grand Ordonnateur.
		// - envoi du nom de notre équipe
		tcpGdOrdo.envoiChaine(nomEquipe);
		// - envoi de l'IUT où les membres de notre équipe étudient
		tcpGdOrdo.envoiChaine(nomIUT);
		// Initialisation du protocole du Grand Ordonnateur.
		// - réception du nombre de lignes de la cave
		int h = tcpGdOrdo.receptionEntier();
		// - réception du nombre de colonnes de la cave
		int w = tcpGdOrdo.receptionEntier();
		// - réception des casiers (nombre d'emplacements et position de
		
		// l'escalier) de la cave
		String chaineCasier = tcpGdOrdo.receptionChaine();

		// - réception du nombre de manutentionnaires ; 3 ici
		nbJoueurs = tcpGdOrdo.receptionEntier();
		// - réception de l'ordre (entre 1 et le nombre de manutentionnaires)

		// dans lequel l'application singeant notre manutentionnaire joue ; 2
		// ici
		place = tcpGdOrdo.receptionEntier();

		tour= 1;

        jeu = new Jeu(w, h, place, chaineCasier, nbJoueurs);
        decision = new Decision(jeu, place);
        affichage = new AffichageCarte(jeu.getCasier());
		affichage.setCoeffScore(5);
	}


	/**

	 * Scénario illustrant le protocole du Grand Ordonnateur.
	 *
	 * Cf. TcpGrandOrdonnateur.chaineValidePourTransmission pour connaître les
	 * caractères autorisés par le protocole du Grand Ordonnateur.
	 *
	 * @throws IOException
	 */
	private void scenario() throws IOException {

		//initialisation
        init("418 I'm a Teapot","IUT Charlemagne");

        //Boucle prinipale
        boolean continuer = true;
		while(continuer)
		{
			//Si c'est à nous
		    if(tour % nbJoueurs == place){
		    	//On prend la décision
		    	String action = decision.prendreDecision(); //maj du jeu dans prendreDecision
		    	//on envoie la chaine
	           // tcpGdOrdo.envoiChaine(action);
				for (char a : action.toCharArray()) {
					tcpGdOrdo.envoiCaractere(a);
				}

	            try{
                    affichage.setDistances(decision.getDistances());
                    int[][] points = decision.getTabPoints();
                    double[][] scores = new double[points.length][points[0].length];
                    for (int i =0; i < points.length; i++)
                    {
                        for (int j = 0; j < points[0].length; j++)
                            scores[i][j] = (int) points[i][j];
                    }
                    affichage.setScores(scores);
               } catch(InvalidParameterException e)
               {
                   System.err.println("Erreur de taille de tableaux");
               }
		    }
		    else{
		    	//on recupere l'état du jeu
		    	 String a = tcpGdOrdo.receptionChaine();
		    	 if(a == null || a.equals("destroy")){
		    		 continuer = false;
		    		 continue;
		    	 }
		    	 jeu.miseAJour(tour % nbJoueurs, a);
		    }
		    //on incremente le tour
			affichage.afficherCarte();
		    tour++;
		}


		// - déconnexion
		try {
			tcpGdOrdo.deconnexion();
		} catch (IOException e) {
			// Qu'importe si le serveur s'est arrêté avant le client.
		}
	}

	/**
	 * Programme principal.
	 *
	 * @param args
	 *            Arguments.
	 */
	public static void main(String[] args) {

		System.out.println("Démarrage de notre application Java ce "
				+ (DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL)).format(new Date()) + ".");
		System.out.flush();
		// « create » du protocole du Grand Ordonnateur.
		final String USAGE = System.lineSeparator() + "\tUsage : java " + Main.class.getName()
				+ " <hôte> <port>";
		if (args.length != 2) {
			System.out.println("Nombre de paramètres incorrect." + USAGE);

			System.out.flush();
			System.exit(1);
		}
		String hote = args[0];
		int port = -1;
		try {
			port = Integer.valueOf(args[1]);
		} catch (NumberFormatException e) {

			System.out.println("Le port doit être un entier." + USAGE);			System.out.flush();
			System.exit(1);
		}
		try {
			(new Main(hote, port)).scenario();
		} catch (IOException e) {
			System.out.println("Erreur lors de la création d'une illustration du protocole du Grand Ordonnateur.");
			System.out.flush();
			e.printStackTrace();
			System.err.flush();
			System.exit(1);
		}
	}

}
