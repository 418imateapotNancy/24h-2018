
public class Coordonnees{
		public int x, y;
		public Coordonnees(int px, int py)
		{
			x = px; y = py;
		}
		
		public boolean equals(Object o)
		{
			if (o == null || !(o instanceof Coordonnees))
				return false;
			Coordonnees c = (Coordonnees) o;
			return c.x == x && c.y == y;
		}
	}