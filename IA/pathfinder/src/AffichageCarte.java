import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class AffichageCarte extends JPanel{

		private class Historique
		{
			private List<List<Object>> listes;
			private List<Integer> indices;
			
			public Historique(int nb)
			{
				listes = new ArrayList<List<Object>>();
				indices = new ArrayList<Integer>();
				
				for (int i = 0; i < nb; i++)
				{
					listes.add(new ArrayList<Object>());
					indices.add(-1);
				}
			}
			
			public Object get(int type, int index)
			{
				try{
					List<Object> l = listes.get(type);
					return l.get(index);
				} catch (IndexOutOfBoundsException e)
				{
					return null;
				}
			}
			
			public Object get(int type)
			{
				try{
					return get(type, indices.get(type));
				} catch (IndexOutOfBoundsException e)
				{
					return null;
				}
			}
			
			public int getIndex(int type)
			{
				return indices.get(type);
			}
			
			public int next(int type)
			{
				int i = indices.get(type) + 1;
				if (i >= listes.get(type).size())
					return listes.get(type).size() - 1;
				indices.set(type, i);
				return i;
			}
			
			public int prev(int type)
			{
				int i = indices.get(type) - 1;
				if (i < 0)
					return 0;
				indices.set(type, i);
				return i;
			}
			
			public void add(int type, Object tab)
			{
				indices.set(type, indices.get(type) + 1);
				
				Object cp  = null;
				if (tab instanceof int[][])
				{
					int[][] tabInt = (int[][]) tab;
					int[][] intCp = new int[tabInt.length][tabInt.length > 0 ? tabInt[0].length : 0];
					for (int i = 0; i < tabInt.length; i++)
						for (int j = 0; j < tabInt[i].length; j++)
							intCp[i][j] = tabInt[i][j];
					cp = intCp;
				}
				else if (tab instanceof double[][])
				{
					double[][] tabDouble = (double[][]) tab;
					double[][] doubleCp = new double[tabDouble.length][tabDouble.length > 0 ? tabDouble[0].length : 0];
					for (int i = 0; i < tabDouble.length; i++)
						for (int j = 0; j < tabDouble[i].length; j++)
							doubleCp[i][j] = tabDouble[i][j];
					cp = doubleCp;
				}
				else if (tab instanceof String[][])
				{
					String[][] tabStr = (String[][]) tab;
					String[][] strCp = new String[tabStr.length][tabStr.length > 0 ? tabStr[0].length : 0];
					for (int i = 0; i < tabStr.length; i++)
						for (int j = 0; j < tabStr[i].length; j++)
							strCp[i][j] = tabStr[i][j];
					cp = strCp;
				}
				else
					throw new ClassCastException("Classe non suppportée par l'historique.");
				
				listes.get(type).add(cp);
			}
			
			public int size()
			{
				return listes.size();
			}
			
			public int size(int type)
			{
				return listes.get(type).size();
			}
		}
		
		private class Outils extends JPanel{
			
			public Outils()
			{
				setPreferredSize(new Dimension(0,30));
				setBorder(BorderFactory.createLineBorder(Color.black));
			}
			
			public void paintComponent(Graphics g)
			{
				super.paintComponent(g);
				int w = getWidth(), caseW = w / NB_TYPES;
				int h = getHeight();
				
				String typeStr[] = {"Score", "Distance", "Type", "Chemin"};
				
				FontMetrics fm = g.getFontMetrics();
				for (int i = 0; i < NB_TYPES; i++)
				{
					Color a = editing.get(i) || indexEditing == i ? Color.white : Color.lightGray ;
					Color b = new Color(255 - a.getRed(), 255 - a.getBlue(), 255 - a.getGreen());
					
					g.setColor(a);
					int x = i * caseW, y = 5;
					int width = caseW - 5, height = h - 5;
					g.fillRect(x, y, width, height);
					g.setColor(b);
					
					String str = typeStr[i] + " : " + (historique.getIndex(i) == -1 ? "Pas de données" : historique.getIndex(i));
					int textW = fm.stringWidth(str), textH = fm.getHeight();
					g.drawString(str, x + width/2 - textW / 2, y + height/2 + textH/2);
					
					if (indexEditing == i)
					{
						g.setColor(new Color(100,0,0));
						g.drawRect(x, y, width, height);
					}
				}
				
			}
		}
		
		private Carte c;
		private JFrame fenetre;
		private List<Color> colors;		
		private int[] affichage;
		private Historique historique;
		private double coeffScore, coeffDist;
		private List<Boolean> editing;
		private int indexEditing;
		private Outils outils;
		private Pathfinding pathfinder;
		
		public static final int AFFICHER_SCORE = 0, AFFICHER_DISTANCE = 1, AFFICHER_TYPES = 2, AFFICHER_CHEMIN = 3;
		public static final int AFFICHAGE_NOMBRE = 0, AFFICHAGE_COULEUR = 1;
		public static final int HISTORY_SCORES = 0, HISTORY_DISTANCES = 1, HISTORY_TYPES = 2, HISTORY_CHEMIN = 3;
		public static final int NB_TYPES = 4;
		
		public AffichageCarte(Carte c)
		{
			this.c = c;
			colors = new ArrayList<Color>();
			colors.add(Color.lightGray);
			colors.add(Color.black);
			colors.add(Color.green);
			colors.add(Color.blue);
			colors.add(Color.red);
			colors.add(Color.yellow);
			colors.add(Color.magenta);
			colors.add(Color.gray);
			
			pathfinder = new Pathfinding(c);
			
			editing = new ArrayList<Boolean>();
			indexEditing = 0;
			for(int i = 0; i < NB_TYPES; i++)
				editing.add(false);
			
			fenetre = new JFrame("Affichage de la carte");
			fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			fenetre.setLayout(new BorderLayout(0,5));
			
			outils = new Outils();
			fenetre.add(outils, BorderLayout.NORTH);
			
			fenetre.add(this, BorderLayout.CENTER);
			fenetre.setSize(c.getWidth() * 40, c.getHeight() *40);
			fenetre.setVisible(true);
			
			historique = new Historique(NB_TYPES);
			saveType();
			
			affichage = new int[] {AFFICHER_DISTANCE, AFFICHER_TYPES};
			
			setFocusable(true);
			requestFocusInWindow();
			
			addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent e)
				{
					int type = AFFICHAGE_NOMBRE;
					if (e.isControlDown())
						type = AFFICHAGE_COULEUR;
					
					if (e.getKeyCode() == KeyEvent.VK_S)
						setAffichage(AFFICHER_SCORE, type);
					else if  (e.getKeyCode() == KeyEvent.VK_D)
						setAffichage(AFFICHER_DISTANCE, type);
					else if  (e.getKeyCode() == KeyEvent.VK_T)
						setAffichage(AFFICHER_TYPES, type);
					else if  (e.getKeyCode() == KeyEvent.VK_C)
						setAffichage(AFFICHER_CHEMIN, type);
					else if  (e.getKeyCode() == KeyEvent.VK_ENTER)
						editing.set(indexEditing, !editing.get(indexEditing));
					else if (e.getKeyCode() == KeyEvent.VK_RIGHT)
						setEditing(indexEditing + 1);
					else if (e.getKeyCode() == KeyEvent.VK_LEFT)
						setEditing(indexEditing + NB_TYPES - 1);
					else if (e.getKeyCode() == KeyEvent.VK_UP)
					{
						historique.next(indexEditing);
						for (int t = 0; t < NB_TYPES; t++)
							if (editing.get(t) && t != indexEditing)
								historique.next(t);
					}
					else if (e.getKeyCode() == KeyEvent.VK_DOWN)
					{
						historique.prev(indexEditing);
						for (int t = 0; t < NB_TYPES; t++)
							if (editing.get(t) && t != indexEditing)
								historique.prev(t);
					}
					
					fenetre.repaint();
				}
			});
			
			coeffScore = 10;
		}
		
		private void setEditing(int index)
		{
			indexEditing = index % NB_TYPES;
		//	editing.set(indexEditing, true);
			//for (int i = 0; i < NB_TYPES; i++)
				//editing.set(i, false);
		}
		
		public void setCoeffScore(double c)
		{
			coeffScore = c;
		}
		
		public void setPath(int x1, int y1, int x2, int y2, int index)
		{
		//	if (x1c.getWidth() || dist[0].length != c.getHeight())
			//	throw new InvalidParameterException("Carte and distances length mismatch");
			List<List<Integer>> listeChemins = pathfinder.directionsPourAller(x1, y1, x2, y2);
			String[][] dirsStr = new String[c.getWidth()][c.getHeight()];
			for (int i = 0; i < c.getWidth(); i++)
				for (int j = 0; j < c.getHeight(); j++)
					dirsStr[i][j] = new String();
			
			int[][] cases = {{0,-1},{1,0},{0,1},{-1,0}};
			char[] dirToStr = {'I', 'C', 'N', 'O', 'S', 'E'};
			
			//Si un index est specifie, on ne prend que le chemin a cet index
			if (index != -1)
			{
				List<Integer> chemin = listeChemins.get(index);
				listeChemins = new ArrayList<List<Integer>>();
				listeChemins.add(chemin);
			}
			
			for (List<Integer> chemin : listeChemins)
			{
				int x = x1, y = y1, lastDir = -1;
				for (int dir : chemin)
				{
					lastDir = dir;
					if (!dirsStr[x][y].contains(""+dirToStr[dir + 2]))
					{
						if (!dirsStr[x][y].isEmpty())
							dirsStr[x][y] += "/";
						dirsStr[x][y] += dirToStr[dir + 2];
					}
					if (dir >= 0)
					{
						x += cases[dir][0];
						y += cases[dir][1];
					}
					else
						break;
				}
				if (lastDir >= 0)
					dirsStr[x][y] = "A";
			}
				
			historique.add(HISTORY_CHEMIN, dirsStr);
		}
		
		public void saveType()
		{
			historique.add(HISTORY_TYPES, c.getRawData());
		}
		
		public void setDistances(int[][] dist) throws InvalidParameterException
		{
			if (dist.length != c.getWidth() || dist[0].length != c.getHeight())
				throw new InvalidParameterException("Carte and distances length mismatch");
			historique.add(HISTORY_DISTANCES, dist);
			calcCoeffDist();
		}
		
		public void setScores(double[][] scores) throws InvalidParameterException
		{
			if (scores.length != c.getWidth() || scores[0].length != c.getHeight())
				throw new InvalidParameterException("Carte and scores length mismatch");
			historique.add(HISTORY_SCORES, scores);
		}
		
		public void afficherCarte()
		{
			repaint();
		}
		
		public void setAffichage(int a, int t)
		{
			affichage[t] = a;
			repaint();
		}
		
		private void calcCoeffDist()
		{
			int[][] distances = (int[][]) historique.get(HISTORY_DISTANCES);
			if (distances != null)
			{
				double max = 0; 
				for (int[] col : distances)
					for (int c : col)
						if (c > max)
							max = c;
				
				coeffDist = (255 - 10) / max;
			}
		}
		
		public void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			
			int w = getWidth(), h = getHeight();
			int carteW = c.getWidth(), carteH = c.getHeight();
			int caseW = w / carteW, caseH = h / carteH;
			

			int[][] distances = (int[][]) historique.get(HISTORY_DISTANCES);
			double[][] scores = (double[][])  historique.get(HISTORY_SCORES);
			int[][] types = (int[][]) historique.get(HISTORY_TYPES);
			String[][] chemin = (String[][]) historique.get(HISTORY_CHEMIN);
			
			for (int i = 0; i < carteW; i++)
			{
				for (int j = 0; j < carteH; j++)
				{
					int type = types == null ? -99 : types[i][j];
					int x = i * caseW, y = j * caseH;
					g.setColor(Color.black);
					g.drawRect(x, y, caseW, caseH);
					
					switch (affichage[AFFICHAGE_COULEUR])
					{
					case AFFICHER_DISTANCE:
						if (distances != null)
						{
							int v = distances[i][j];
							v = (int) (v == -1 ? 0 : 255 - (v * coeffDist));
							v = v > 255 ? 255 : v < 0 ? 0 : v;
							g.setColor(new Color(v,v,v));
							g.fillRect(x + 1, y + 1, caseW - 1, caseH - 1);
							g.setColor(new Color(200, 200, 0));
						}
						break;
					case AFFICHER_SCORE:
						if (scores != null)
						{
							int r = (int)(scores[i][j] * coeffScore);
							r = (r != 0 ?r + 40 : 0);
							r = r > 255 ? 255 : r < 0 ? 0 : r;
							g.setColor(new Color(r,0,0));
							g.fillRect(x + 1, y + 1, caseW - 1, caseH - 1);
							g.setColor(Color.white);
						}
						break;
					case AFFICHER_TYPES:
						Color c = Color.black;
						try{
							 c = colors.get(type);
						}catch(IndexOutOfBoundsException e)
						{
							
						}
						g.setColor(c);
						g.fillRect(x + 1, y + 1, caseW - 1, caseH - 1);
						g.setColor(c == Color.black ? Color.lightGray : Color.black);
						break;
					case AFFICHER_CHEMIN:
						if (chemin != null)
						{
							c = chemin[i][j].isEmpty() ? Color.black : Color.white;
							g.setColor(c);
							g.fillRect(x + 1, y + 1, caseW - 1, caseH - 1);
							g.setColor(c == Color.black ? Color.lightGray : Color.black);						
						}
						break;
					}
					
					String str = "-";
					switch(affichage[AFFICHAGE_NOMBRE])
					{
						case AFFICHER_TYPES:
							str = "" + type;
							break;
						case AFFICHER_DISTANCE:
							if (distances != null)
								str = "" + distances[i][j];
							break;
						case AFFICHER_SCORE:
							if(scores != null)
								str = "" + Math.round(scores[i][j] * 100) / 100.0;
							break;
						case AFFICHER_CHEMIN:
							if (chemin != null)
								str = chemin[i][j];
					}
					
					FontMetrics fm = g.getFontMetrics();
					int wText = fm.stringWidth(str), hText = fm.getHeight();
					g.drawString(str, x + caseW/2 - wText/2, y + caseH/2 + hText/2);
				}
			}
		}
}
