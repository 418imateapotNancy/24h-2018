
public interface Carte {

		public int getWidth();
		public int getHeight();
		
		public boolean estTraversable(int x, int y);
		
		public int getTypeCase(int x, int y);
		
		public boolean aChange();
		
		public int[][] getRawData();
		
}
