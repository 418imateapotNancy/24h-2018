import java.util.Arrays;

public class Carte2d implements Carte {
	
	private int[][] carte;
	boolean change;
	
	public Carte2d(int w, int h)
	{
		w = w < 1 ? 1 : w;
		h = h < 1 ? 1 : h;
		carte = new int[w][h];
		for (int[] row : carte)
			Arrays.fill(row, 0);
		change = false;
	}
	
	public void set(int x, int y, int type)
	{
		carte[x][y] = type;
		change = true;
	}
	
	public int get(int x, int y)
	{
		return carte[x][y];
	}
	
	@Override
	public int getWidth() {
		return carte.length;
	}

	@Override
	public int getHeight() {
		return carte.length == 0 ? 0 : carte[0].length;
	}

	@Override
	public boolean estTraversable(int x, int y) {
		return getTypeCase(x,y) == 0;
	}

	@Override
	public int getTypeCase(int x, int y) {
		return carte[x][y];
	}

	@Override
	public boolean aChange() {
		boolean res = change;
		change = false;
		return res;
	}

	@Override
	public int[][] getRawData() {
		return carte;
	}

}
