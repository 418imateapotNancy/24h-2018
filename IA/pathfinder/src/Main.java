
public class Main {

	public static void main(String[] args) {
		Carte2d c = new Carte2d(50,15);
		
		for (int i = 0; i < c.getWidth(); i++)
		{
			for (int j = 0; j < c.getHeight(); j++)
			{
				c.set(i, j, (int) (Math.random() * 1.5));
			}
		}
		
		c.set(5, 5, 0);
		
		AffichageCarte affichage = new AffichageCarte(c);
		
		Pathfinding p = new Pathfinding(c);
		try{

			affichage.setDistances(p.calculerDistances(8, 5));
			affichage.setPath(5, 5, 8, 5, 0);
			
			affichage.setDistances(p.calculerDistances(40, 2));
			affichage.setPath(12, 5, 40, 2, 0);

			affichage.setDistances(p.calculerDistances(2, 5));
			affichage.setPath(48, 13, 2, 5, 0);
			
			affichage.setDistances(p.calculerDistances(10, 5));
			affichage.setPath(2, 1, 10, 5, -1);
			
			int[][] dist = p.calculerDistances(5, 5);
			double[][] scores = new double[dist.length][dist[0].length];
			affichage.setDistances(dist);
			for (int i = 0; i < dist.length; i++)
			{
				for (int j = 0; j < dist[i].length; j++)
				{
					scores[i][j] = dist[i][j] == -1 ? 0 : dist[i][j] == 0 ? 205 : 50.0 / dist[i][j];
				}
			}
		}catch(InvalidParameterException e) {System.err.println("Error:"+ e);};
		
	}

}
