import java.util.*;

public class Pathfinding {
	
	private Carte carte;
	private HashMap<Coordonnees, int[][]> cachedDist;
	
	public static final int DIRECTION_NORD = 0, DIRECTION_EST = 1, DIRECTION_SUD = 2, DIRECTIONS_OUEST = 3, DIRECTION_CENTRE = -1, DIRECTION_IMPOSSIBLE = -2;
	
	public Pathfinding(Carte c)
	{
		cachedDist = new HashMap<Coordonnees, int[][]>();
		setCarte(c);
	}

	public void setCarte(Carte c)
	{
		if (c == null)
			throw new NullPointerException("La carte est null");
		this.carte = c;
		cachedDist.clear();
	}
	
	private void cacheDist(int x, int y, int[][] dist)
	{
		cachedDist.put(new Coordonnees(x,y), dist);
	}
	
	private int[][] getCachedDist(int x, int y)
	{
		if (carte.aChange())
			cachedDist.clear();
		return cachedDist.get(new Coordonnees(x,y));
	}
	
	public int[][] calculerDistances(int caseX, int caseY)
	{

		if (caseX<0 || caseY<0 || caseX>=carte.getWidth() || caseY>=carte.getHeight())
			return null;
		
		int[][] dist = getCachedDist(caseX, caseY);
		if (dist != null)
			return dist;
			
		dist = new int[carte.getWidth()][carte.getHeight()];
		
		for (int i = 0; i < dist.length; i++)
			Arrays.fill(dist[i], -1);

		int step = 0;
		boolean plein = false;
		dist[caseX][caseY] = step;

		ArrayList<int[]> lastIndices = new ArrayList<int[]>();
		lastIndices.add(new int[] {caseX, caseY});

		int[][] cases = {{0, -1}, {1, 0}, {0, 1}, {-1, 0}};
		while (!plein)
		{
			ArrayList<int[]> loopIndices = new ArrayList<int[]>();
			step++;
			plein = true;
			for (int c = 0; c < lastIndices.size(); c++)
			{
				for (int i = 0; i < cases.length; i++)
				{
					int x = cases[i][0] + lastIndices.get(c)[0], y = cases[i][1] + lastIndices.get(c)[1];

					if (x<0 || y<0 || x>=carte.getWidth() || y>=carte.getHeight())
						continue;
					//Si c'est pas un mur et qu'on l'a pas déja exploré
					if (carte.estTraversable(x, y) && dist[x][y] == -1)
					{
						dist[x][y] = step;
						loopIndices.add(new int[] {x, y});
						plein = false;
					}
				}
			}
			lastIndices = loopIndices;
		}
		cacheDist(caseX, caseY, dist);
		return dist;
	}

	/**
	 * Retourne une liste des chemins possibles les plus courts pour aller d'un point A a un point B, en tenantn compte des cases non traversables.
	 * Le chemin est une liste de cases, identifiee par ses coordonnees (classe Coordonnees)
	 * @param xDebut Abscisse de l'origine
	 * @param yDebut Ordonnee de l'origine
	 * @param xFin Abscisse du but
	 * @param yFin Ordonnee du but
	 * @return La liste des chemins
	 */
	public List<List<Coordonnees>> cheminPourAller(int xDebut, int yDebut, int xFin, int yFin)
	{
		//Mouvements corrspondants aux directions
		int[][] cases = {{0, -1}, {1, 0}, {0, 1}, {-1, 0}};
		//On recupere les directions
		List<List<Integer>> directions = directionsPourAller(xDebut, yDebut, xFin, yFin);
		//On cree notre liste de retour
		List<List<Coordonnees>> chemins = new ArrayList<List<Coordonnees>>();
		
		//Pour tous les chemins possibles
		for (List<Integer> dir : directions)
		{
			//On cree la liste de cases de ce chemin
			ArrayList<Coordonnees> chemin = new ArrayList<Coordonnees>();
			//On ajoute la case actuelle
			chemin.add(new Coordonnees(xDebut, yDebut));
			//On applique tous les mouvements
			for (int d : dir)
			{
				//On prend la position au point actuel
				Coordonnees prev = chemin.get(chemin.size() - 1);
				//On calcule les coordonnees
				int newX = prev.x + cases[d][0];
				int newY = prev.y + cases[d][1];
				//On ajoute la case apres ce mouvement
				chemin.add(new Coordonnees(newX, newY));
			}
			//On ajoute ce chemin a nos chemins
			chemins.add(chemin);
		}
		return chemins;
	}
	
	/**
	 * Retourne une liste de cases qui sont les premieres cases des chemins retournes par {@link #cheminPourAller(int, int, int, int)}
	 */
	public List<Coordonnees> premiereCaseVers(int xDebut, int yDebut, int xFin, int yFin)
	{

		List<Coordonnees> cases = new ArrayList<Coordonnees>();
		List<List<Coordonnees>> listeChemins = cheminPourAller(xDebut, yDebut, xFin, yFin);
		
		for (List<Coordonnees> chemin : listeChemins)
			cases.add(chemin.get(0));
		
		return cases;
	}
	
	/**
	 * Retourne une liste de différents chemins.
	 * Un chemin est une suite de directions (voir constantes DIRECTION_*)
	 * @param xDebut
	 * @param yDebut
	 * @param xFin
	 * @param yFin
	 * @return
	 */
	public List<List<Integer>> directionsPourAller(int xDebut, int yDebut, int xFin, int yFin)
	{
		//Liste de retour: Liste de Chemins possibles (Liste de Directions)
		List<List<Integer>> ret = new ArrayList<List<Integer>>();
		ret.add(new ArrayList<Integer>());
		
		//Si la destination ou l'arrivee n'est pas traversable, alors impossible
		if (!(carte.estTraversable(xDebut, yDebut) && carte.estTraversable(xFin, yFin)))
		{
			ret.get(0).add(DIRECTION_IMPOSSIBLE);
			return ret;
		}
		
		//On recupere les distances a partir de notre point d'arrivee
		int[][] distances = calculerDistances(xFin, yFin);
		// On recupere la distance du point de depart au point d'arrivee
		int dist = distances[xDebut][yDebut];
		
		//Si dist == -1 (impossible) ou si dist == 0 (déjà dessus), on retourne un chemin, avec une direction impossible
		if (dist <= 0)
		{
			ret.get(0).add(dist == -1 ? DIRECTION_IMPOSSIBLE : DIRECTION_CENTRE);
			return ret;
		}

		int[][] cases = {{0, -1}, {1, 0}, {0, 1}, {-1, 0}};
		//Distance qu'on veut avoir (next  case)
		int target = dist - 1;
		
		//Coordonnees du point du chemin ou on est (plusieurs chemins possibles)
		List<Coordonnees> coordonnees = new ArrayList<Coordonnees>();
		
		//Ajout du point de depart
		coordonnees.add(new Coordonnees(xDebut, yDebut));
		//Tant qu'on est pas arrive
		do {
			//Pour tous les chemins explores
			for (int j = 0; j < coordonnees.size(); j++)
			{
				//On track si on a deja ajoute un chemin
				boolean added = false;
				//Coordoonees apres le mouvement
				Coordonnees finalCoords = new Coordonnees(coordonnees.get(j).x, coordonnees.get(j).y);
				//Pour toutes les directions possibles
				for (int i = 0; i < 4; i++)
				{
					//On calcule la nouvelle position
					int x = coordonnees.get(j).x + cases[i][0];
					int y = coordonnees.get(j).y + cases[i][1];
					//On check qu'on sort pas de l'arene
					if (x < 0 || y < 0 || x >= carte.getWidth() || y >= carte.getHeight())
						continue;
					
					//Si cette case est bien la prochaine du chemin
					if (distances[x][y] == target)
					{
						//Si le chemin diverge, on cree un nouveau chemin
						if (added)
						{
							coordonnees.add(new Coordonnees(x,y));
							List<Integer> dirs = new ArrayList<Integer>();
							for (int k = 0; k < ret.get(j).size() - 1; k++)
								dirs.add(ret.get(j).get(k));
							dirs.add(i);
							ret.add(dirs);
						}
						else
						{
							ret.get(j).add(i); // Sinon on ajoute la case au chemin
							//On set les coords apres move
							finalCoords = new Coordonnees(x,y);
							added = true; //On marque qu'on a deja ajoute une case
						}
					}
				}
				//Update des coordonnees
				coordonnees.get(j).x = finalCoords.x;
				coordonnees.get(j).y = finalCoords.y;
			}
		}while (target-- > 0);
		
		return ret;
	}
	
	/**
	 * Retourne une liste de possible direction à prendre pour aller vers le point Fin depuis le point Début.
	 * Cela revient à prendre la premiere directions de tous les chemins de {@link #directionsPourAller(int, int, int, int)} 
	 * @param xDebut
	 * @param yDebut
	 * @param xFin
	 * @param yFin
	 * @return
	 */
	public List<Integer> directionPourAller(int xDebut, int yDebut, int xFin, int yFin)
	{
		List<Integer> dirs = new ArrayList<Integer>();
		List<List<Integer>> listeDirs = directionsPourAller(xDebut, yDebut, xFin, yFin);
		
		for (List<Integer> chemin : listeDirs)
			dirs.add(chemin.get(0));
		
		return dirs;
	}
	
	/**
	 * Recherche les case les plus proches egale a type (distance minimale)
	 * Retourne une liste avec plusieurs element si et seulement si plusieurs cases se trouve a la distance minimale du point Dep
	 * @param xDep Abscisse de debut
	 * @param yDep Ordonne de debut
	 * @param type type a rechercher
	 * @return Une liste des coordonnees des cases trouvees
	 */
	public List<Coordonnees> chercherPlusProche(int xDep, int yDep, int type)
	{
		int[][] distances = calculerDistances(xDep,yDep);

		List<Coordonnees> ret = new ArrayList<Coordonnees>();
		
		int w = carte.getWidth(), h = carte.getHeight();
		int min = w * h;
				
		for (int x = 0; x < w; x++)
		{
			for (int y = 0; y < h; y++)
			{
				//Si la distance n'est pas plus grande que le min actuel et que la case est du bon type
				if (distances[x][y] <= min && carte.getTypeCase(x, y) == type)
				{
					//Si la distance est inferieure au min : on change de min
					if (distances[x][y] < min)
					{
						ret.clear(); //Les cases sont devenus invalides
						min = distances[x][y];
					}
					//On ajoute la case a notre liste de cases
					ret.add(new Coordonnees(x,y));
				}
			}
		}
		return ret;
	
	}
}
